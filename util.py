# RFire ACTIVE LEARNING
# 04/07/20
#
# Works with the range profile code starting with process_dat.py
from scipy.fftpack import fft
import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
import multiprocessing as mp
from multiprocessing import Pool
from os import listdir
import object_factory
import sample_factory
import normalizing
from sklearn.utils import shuffle

globalmaxref = 0
globalminref = 1000000000
globalmaxpercfluc = 0
globalminpercfluc = 100000000000
globalmaxdiff = 0
globalmindiff = 1000000000000
globalmaxdegree = 0
globalmindegree = 360

bin_count = {}

for i in range(200):
    bin_count[i] = 0


def addData(reg_data, hybrid_data, fft_data, could_add):
    X, X_second, X_fft = [], [], []
    # print("in add: ", could_add)
    if could_add > 0:
        if len(reg_data) >= could_add:
            X = reg_data[:could_add]
            X_second = hybrid_data[:could_add]
            X_fft = fft_data[:could_add]
            reg_data = reg_data[could_add:]
            hybrid_data = hybrid_data[could_add:]
            fft_data = fft_data[could_add:]
            # print("X: ", np.array(X).shape, " len: ", len(reg_data), " could: ", could_add)
        elif len(reg_data) > 0:
            could_add = len(reg_data)
            X = reg_data
            X_second = hybrid_data
            X_fft = fft_data
            reg_data, hybrid_data, fft_data = [], [], []
            # print("all new: ", np.array(X).shape, " len: ", len(reg_data), " could: ", could_add)
        else:
            could_add = 0
            # print('couldnt add any: ', len(reg_data))

    return np.array(X), np.array(X_second), np.array(X_fft), could_add, reg_data, hybrid_data, fft_data

def makeLabelsAndJoin(X_fire, X_second_fire, X_fft_fire, X_nonfire, X_second_nonfire, X_fft_nonfire):
    X_fire = np.array(X_fire)
    X_second_fire = np.array(X_second_fire)
    X_fft_fire = np.array(X_fft_fire)

    X_nonfire = np.array(X_nonfire)
    X_second_nonfire = np.array(X_second_nonfire)
    X_fft_nonfire = np.array(X_fft_nonfire)

    fire_zeros = np.full((X_fire.shape[0], 1), 0)
    fire_ones = np.full((X_fire.shape[0], 1), 1)
    fire_labels = np.concatenate((fire_zeros, fire_ones), axis=1)
    nonfire_zeros = np.full((X_nonfire.shape[0], 1), 0)
    nonfire_ones = np.full((X_nonfire.shape[0], 1), 1)
    nonfire_labels = np.concatenate((nonfire_ones, nonfire_zeros), axis=1)

    X = np.concatenate((X_fire, X_nonfire), axis=0)
    X_second = np.concatenate((X_second_fire, X_second_nonfire), axis=0)
    X_fft = np.concatenate((X_fft_fire, X_fft_nonfire), axis=0)
    y = np.concatenate((fire_labels, nonfire_labels), axis=0)

    X, X_second, X_fft, y = shuffle(X, X_second, X_fft, y)
    return X, X_second, X_fft, y


def concatArrays(lst):
    if len(lst) == 0:
        return np.array([])
    concated = lst[0]
    del lst[0]

    for i in lst:
        concated = np.concatenate((concated, i), axis=0)
    print("Concated shape: ", concated.shape)
    return concated


def firstDataset(fire_data, nonfire_data):
    actualfiresamples, actualhybridfire, actualfirefft = fire_data
    nonfirebins, hybridnofirebins, fftnofirebins = nonfire_data
    X_fire, X_nonfire, X_second_fire, X_second_nonfire, X_fft_fire, X_fft_nonfire = [], [], [], [], [], []

    for i, val in enumerate(actualfiresamples.keys()):
        amt = 20
        try:
            if len(actualfiresamples[val]) < len(nonfirebins[val]):
                X_fire_return, X_second_fire_return, X_fft_fire_return, could_add, actualfiresamples[val], actualhybridfire[val], actualfirefft[val] = addData(actualfiresamples[val], actualhybridfire[val], actualfirefft[val], amt)
                # could_add = amt + (amt - could_add)
                X_nonfire_return, X_second_nonfire_return, X_fft_nonfire_return,_, nonfirebins[val], hybridnofirebins[val], fftnofirebins[val] = addData(nonfirebins[val], hybridnofirebins[val], fftnofirebins[val], could_add)
            else:
                X_nonfire_return, X_second_nonfire_return, X_fft_nonfire_return, could_add, nonfirebins[val], hybridnofirebins[val], fftnofirebins[val] = addData(nonfirebins[val], hybridnofirebins[val], fftnofirebins[val], amt)
                # could_add = amt + (amt - could_add)
                X_fire_return, X_second_fire_return, X_fft_fire_return,_, actualfiresamples[val], actualhybridfire[val], actualfirefft[val] = addData(actualfiresamples[val], actualhybridfire[val], actualfirefft[val], could_add)

            if X_fire_return.size > 2:
                X_fire.append(X_fire_return)
                X_second_fire.append(X_second_fire_return)
                X_fft_fire.append(X_fft_fire_return)
            if X_nonfire_return.size > 2:
                X_nonfire.append(X_nonfire_return)
                X_second_nonfire.append(X_second_nonfire_return)
                X_fft_nonfire.append(X_fft_nonfire_return)
        except:
            print("Key: ", val, " is not in nonfire")
            continue

    X_fire = concatArrays(X_fire)
    X_nonfire = concatArrays(X_nonfire)
    X_second_fire = concatArrays(X_second_fire)
    X_second_nonfire = concatArrays(X_second_nonfire)
    X_fft_fire = concatArrays(X_fft_fire)
    X_fft_nonfire = concatArrays(X_fft_nonfire)

    print("x fire: ", X_fire.shape)
    print("x noonfiire: ", X_nonfire.shape)
    X, X_second, X_fft, y = makeLabelsAndJoin(X_fire, X_second_fire, X_fft_fire, X_nonfire, X_second_nonfire, X_fft_nonfire)
    fire_data = [actualfiresamples, actualhybridfire, actualfirefft] #actualhybridfirefft
    nonfire_data = [nonfirebins, hybridnofirebins, fftnofirebins]
    return X, X_second, X_fft, y, fire_data, nonfire_data

def addFinalData(X, X_second, X_fft, y_train, fire_data, nonfire_data):
    actualfiresamples, actualhybridfire, actualfirefft = fire_data
    nonfirebins, hybridnofirebins, fftnofirebins = nonfire_data
    X_add, X_second_add, X_fft_add, new_y = [], [], [], []
    X_add_nonfire, X_second_add_nonfire, X_fft_add_nonfire, new_y_nonfire = [], [], [], []

    nonfires_needed = 0
    added_fires = 0
    print("in add final data")

    for i, val in enumerate(actualfiresamples.keys()):
        try:
            if len(actualfiresamples[val]) > 0 and len(actualfiresamples[val]) <= len(nonfirebins[val]):
                X_add.append(actualfiresamples[val])
                X_second_add.append(actualhybridfire[val])
                X_fft_add.append(actualfirefft[val])
                X_add_nonfire.append(nonfirebins[val][:len(actualfiresamples[val])])
                X_second_add_nonfire.append(hybridnofirebins[val][:len(actualfiresamples[val])])
                X_fft_add_nonfire.append(fftnofirebins[val][:len(actualfiresamples[val])])
                added_fires+=len(actualfiresamples[val])
                print("len of fire less: ", len(actualfiresamples[val]), " than nonfire: ", len(nonfirebins[val]))
            elif len(actualfiresamples[val]) > 0 and len(actualfiresamples[val]) > len(nonfirebins[val]):
                X_add_nonfire.append(nonfirebins[val])
                X_second_add_nonfire.append(hybridnofirebins[val])
                X_fft_add_nonfire.append(fftnofirebins[val])
                X_add.append(actualfiresamples[val][:len(nonfirebins[val])])
                X_second_add.append(actualhybridfire[val][:len(nonfirebins[val])])
                X_fft_add.append(actualfirefft[val][:len(nonfirebins[val])])
                nonfires_needed+=(len(actualfiresamples[val]) - len(nonfirebins[val]))
                added_fires+=len(nonfirebins[val])
                print("len of nonfire less: ", len(nonfirebins[val]), " than fire: ", len(actualfiresamples[val]))
            else:
                print("len of fires for val ", val, " : ", len(actualfiresamples[val]))
        except:
            print("Key: ", val, " is not in fire samples")
            continue

    nonfires_left = 0
    for i, val in enumerate(nonfirebins.keys()):
        nonfires_left+=len(nonfirebins[val])

    print("added fires: ", added_fires)
    print("Nonfires left: ", nonfires_left)

    while nonfires_needed > 0 and nonfires_left > 0:
        nonfires_left = 0
        for i, val in enumerate(nonfirebins.keys()):
            X_add_nonfire.append(nonfirebins[val][0])
            X_second_add_nonfire.append(hybridnofirebins[val][0])
            X_fft_add_nonfire.append(fftnofirebins[val][0])
            nonfirebins[val] = nonfirebins[val][1:]
            hybridnofirebins[val] = hybridnofirebins[val][1:]
            fftnofirebins[val] = fftnofirebins[val][1:]
            nonfires_needed-=1
            nonfires_left+=len(nonfirebins[val])
        print("186 - Nonfires left: ", nonfires_left)

    X_add = concatArrays(X_add)
    X_second_add = concatArrays(X_second_add)
    X_fft_add = concatArrays(X_fft_add)
    X_add_nonfire = concatArrays(X_add_nonfire)
    X_second_add_nonfire = concatArrays(X_second_add_nonfire)
    X_fft_add_nonfire = concatArrays(X_fft_add_nonfire)

    fire_zeros = np.full((X_add.shape[0], 1), 0)
    fire_ones = np.full((X_add.shape[0], 1), 1)
    new_y = np.concatenate((fire_zeros, fire_ones), axis=1)

    nonfire_zeros = np.full((X_add_nonfire.shape[0], 1), 0)
    nonfire_ones = np.full((X_add_nonfire.shape[0], 1), 1)
    new_y_nonfire = np.concatenate((nonfire_ones, nonfire_zeros), axis=1)


    X_add = np.concatenate((X_add, X_add_nonfire), axis=0)
    X_second_add = np.concatenate((X_second_add, X_second_add_nonfire), axis=0)
    X_fft_add = np.concatenate((X_fft_add, X_fft_add_nonfire), axis=0)
    new_y = np.concatenate((new_y, new_y_nonfire), axis=0)

    if X_add.size > 2:
        X = np.concatenate((X_add, X), axis=0)
        X_second = np.concatenate((X_second_add, X_second), axis=0)
        X_fft = np.concatenate((X_fft_add, X_fft), axis=0)
        y_train = np.concatenate((new_y, y_train), axis=0)
        X, X_second, X_fft, y_train = shuffle(X, X_second, X_fft, y_train)

    print("X: ", X.shape)
    print("y_train: ", y_train.shape)

    return X, X_second, X_fft, y_train



def nextDataset(fire_data, nonfire_data):
    print(">NEXT DATASET")
    actualfiresamples, actualhybridfire, actualfirefft = fire_data
    nonfirebins, hybridnofirebins, fftnofirebins = nonfire_data
    XA_test_nonfire, XA_second_nonfire, XA_fft_nonfire = [], [], []
    XA_test_fire, XA_second_fire, XA_fft_fire = [], [], []
    reg_amt = 100
    amt = reg_amt
    for i, val in enumerate(nonfirebins.keys()):
        X_nonfire_return, X_second_nonfire_return, X_fft_nonfire_return, could_add, nonfirebins[val], hybridnofirebins[val], fftnofirebins[val] = addData(nonfirebins[val], hybridnofirebins[val], fftnofirebins[val], amt)
        if X_nonfire_return.size > 2:
            XA_test_nonfire.append(X_nonfire_return)
            XA_second_nonfire.append(X_second_nonfire_return)
            XA_fft_nonfire.append(X_fft_nonfire_return)
        amt = reg_amt + (reg_amt - could_add)

    XA_test_nonfire = concatArrays(XA_test_nonfire)
    XA_second_nonfire = concatArrays(XA_second_nonfire)
    XA_fft_nonfire = concatArrays(XA_fft_nonfire)
    nonfire_labels = makeLabels(XA_test_nonfire, mode="nonfire")
    print(">new nonfires: ", XA_test_nonfire.shape)

    if XA_test_nonfire.shape[0] == 0:
        nonfire_data = [nonfirebins, hybridnofirebins, fftnofirebins]
        fire_data = [actualfiresamples, actualhybridfire, actualfirefft]
        return XA_test_nonfire, XA_second_nonfire, XA_fft_nonfire, nonfire_labels, nonfire_data, fire_data

    # reg_amt = 50
    # amt = reg_amt
    # added = 0
    # for i, val in enumerate(actualfiresamples.keys()):
    #     X_fire_return, X_second_fire_return, X_fft_fire_return, could_add, actualfiresamples[val], actualhybridfire[val], actualfirefft[val] = addData(actualfiresamples[val], actualhybridfire[val], actualfirefft[val], amt)
    #     if X_fire_return.size > 2:
    #         XA_test_fire.append(X_fire_return)
    #         XA_second_fire.append(X_second_fire_return)
    #         XA_fft_fire.append(X_fft_fire_return)
    #         added+=X_fire_return.shape[0]
    #         if added > XA_test_nonfire.shape[0]:
    #             break
    #     amt = reg_amt + (reg_amt - could_add)
    #
    # XA_test_fire = concatArrays(XA_test_fire)
    # XA_second_fire = concatArrays(XA_second_fire)
    # XA_fft_fire = concatArrays(XA_fft_fire)
    # fire_labels = makeLabels(XA_test_fire, mode="fire")
    # XA_test_fire = XA_test_fire[:XA_test_nonfire.shape[0]]
    # XA_second_fire = XA_second_fire[:XA_test_nonfire.shape[0]]
    # XA_fft_fire = XA_fft_fire[:XA_test_nonfire.shape[0]]
    # fire_labels = fire_labels[:XA_test_nonfire.shape[0]]
    # print(">new fires: ", XA_test_fire.shape)
    #
    # if XA_test_fire.size == 0:
    #     nonfire_data = [nonfirebins, hybridnofirebins, fftnofirebins]
    #     fire_data = [actualfiresamples, actualhybridfire, actualfirefft]
    #     return XA_test_nonfire, XA_second_nonfire, XA_fft_nonfire, nonfire_labels, nonfire_data, fire_data

    # XA_test = np.concatenate((XA_test_nonfire, XA_test_fire), axis=0)
    # XA_second = np.concatenate((XA_second_nonfire, XA_second_fire), axis=0)
    # XA_fft = np.concatenate((XA_fft_nonfire, XA_fft_fire), axis=0)
    # labels = np.concatenate((nonfire_labels, fire_labels), axis=0)
    XA_test, XA_second, XA_fft, labels = shuffle(XA_test_nonfire, XA_second_nonfire, XA_fft_nonfire, nonfire_labels) #XA_test, XA_second, XA_fft, labels)
    print(">together: ", XA_test.shape)

    nonfire_data = [nonfirebins, hybridnofirebins, fftnofirebins]
    fire_data = [actualfiresamples, actualhybridfire, actualfirefft]
    return XA_test, XA_second, XA_fft, labels, nonfire_data, fire_data


def getBal(get_samples, wrong_X, actualsamples, actualHybrid, actualFFT):
    X_bal, X_second_bal, X_fft_bal = [], [], []
    distance_idx = 13
    need_samp = 0
    added_samp = 0
    for i, val in enumerate(wrong_X):
        dist = round(val[0][distance_idx], 3)
        try:
            if len(actualsamples[dist]) > 0:
                X_bal.append(actualsamples[dist][0])
                X_second_bal.append(actualHybrid[dist][0])
                X_fft_bal.append(actualFFT[dist][0])
                actualsamples[dist] = actualsamples[dist][1:]
                actualHybrid[dist] = actualHybrid[dist][1:]
                actualFFT[dist] = actualFFT[dist][1:]
                added_samp+=1
            # else:
            #     need_samp+=1

            if added_samp == get_samples:
                need_samp = 0
                break
        except:
            # print("Key: ", dist, " no more fire samples")
            continue

    samps_left = 0
    for i, val in enumerate(actualsamples.keys()):
        samps_left+=len(actualsamples[val])

    need_samp = len(wrong_X) - added_samp

    return samps_left, need_samp, added_samp, X_bal, X_second_bal, X_fft_bal, actualsamples, actualHybrid, actualFFT

def secondaryBal(need_samps, samps_left, actualsamples, actualhybrid, actualfft, X_bal, X_second_bal, X_fft_bal, wrong_X, wrong_X_second, wrong_X_fft):
    if need_samps > 0:
        while need_samps > 0 and samps_left > 0:
            for i, val in enumerate(actualsamples.keys()):
                if len(actualsamples[val]) > 0:
                    X_bal.append(actualsamples[val][0])
                    X_second_bal.append(actualhybrid[val][0])
                    X_fft_bal.append(actualfft[val][0])
                    actualsamples[val] = actualsamples[val][1:]
                    actualhybrid[val] = actualhybrid[val][1:]
                    actualfft[val] = actualfft[val][1:]
                    need_samps-=1
                if need_samps == 0:
                    break
            samps_left = 0
            for i, val in enumerate(actualsamples.keys()):
                samps_left+=len(actualsamples[val])
            print("366 - samps left: ", samps_left)

    X_bal = np.array(X_bal)
    X_second_bal = np.array(X_second_bal)
    X_fft_bal = np.array(X_fft_bal)

    print("X_bal: ", X_bal.shape)
    # if X_bal.shape[0] < wrong_X.shape[0]:
    #     wrong_X = wrong_X[:X_bal.shape[0]]
    #     wrong_X_second = wrong_X_second[:X_bal.shape[0]]
    #     wrong_X_fft = wrong_X_fft[:X_bal.shape[0]]
    #     print("new wrong shape: ", wrong_X.shape)

    return X_bal, X_second_bal, X_fft_bal, actualsamples, actualhybrid, actualfft, wrong_X, wrong_X_second, wrong_X_fft

def makeLabels(shape_arr, mode="fire"):
    if mode == "fire":
        fire_zeros = np.full((shape_arr.shape[0], 1), 0)
        fire_ones = np.full((shape_arr.shape[0], 1), 1)
        return np.concatenate((fire_zeros, fire_ones), axis=1)
    else:
        nonfire_zeros = np.full((shape_arr.shape[0], 1), 0)
        nonfire_ones = np.full((shape_arr.shape[0], 1), 1)
        return np.concatenate((nonfire_ones, nonfire_zeros), axis=1)

def makeNewLabels(X_bal, X_second_bal, X_fft_bal, wrong_X, wrong_X_second, wrong_X_fft, X, X_second, X_fft, y_train, bal_mode="fire"):
    if bal_mode == "fire": #balancing with fires
        bal_labels = makeLabels(X_bal, mode=bal_mode)
        wrong_labels = makeLabels(wrong_X, mode="nonfire")
    else: #balancing with nonfires
        bal_labels = makeLabels(X_bal, mode=bal_mode)
        wrong_labels = makeLabels(wrong_X, mode="fire")

    print(">Balance (should be same): ", X_bal.shape, " wrong: ", wrong_X.shape)
    X_new = np.concatenate((X_bal, wrong_X), axis=0)
    X_second_new = np.concatenate((X_second_bal, wrong_X_second), axis=0)
    X_fft_new = np.concatenate((X_fft_bal, wrong_X_fft), axis=0)
    y_new = np.concatenate((bal_labels, wrong_labels), axis=0)

    #concat with original dataset
    if X_new.size > 0:
        X = np.concatenate((X, X_new), axis=0)
        X_second = np.concatenate((X_second, X_second_new), axis=0)
        X_fft = np.concatenate((X_fft, X_fft_new), axis=0)
        y_train = np.concatenate((y_train, y_new), axis=0)

    return X, X_second, X_fft, y_train

def activeDataset(fire_data, nonfire_data, X, X_second, X_fft, y_train, XA_test, XA_second, XA_fft, yA_test, y_predict):
    print("ACTIVE DATASET")
    if XA_test.size < 2:
        return X, X_second, X_fft, y_train, fire_data, nonfire_data
    actualfiresamples, actualhybridfire, actualfirefft = fire_data
    nonfirebins, hybridnofirebins, fftnofirebins = nonfire_data
    wrong_X, wrong_X_second, wrong_X_fft = [], [], []
    # print("yA_test: ", yA_test)
    fire_wrong, nonfire_wrong = [], []
    for i, val in enumerate(yA_test):
        if np.argmax(yA_test[i]) != np.argmax(y_predict[i]):
            wrong_X.append(XA_test[i])
            wrong_X_second.append(XA_second[i])
            wrong_X_fft.append(XA_fft[i])
# _____added
    #         if np.argmax(yA_test[i]) == 0:
    #             nonfire_wrong.append(i)
    #         else:
    #             fire_wrong.append(i)
    #
    # if len(fire_wrong) > len(nonfire_wrong):
    #     get_nonfires = len(fire_wrong) - len(nonfire_wrong)
    #     get_fires = 0
    #     print("Need NONFIRES: ", get_nonfires)
    # else:
    #     get_fires = len(nonfire_wrong) - len(fire_wrong)
    #     get_nonfires = 0
    #     print("Need FIRES: ", get_fires)
    #
    # fire_wrong_X = XA_test[fire_wrong]
    # fire_wrong_X_second = XA_second[fire_wrong]
    # fire_wrong_X_fft = XA_fft[fire_wrong]
    # nonfire_wrong_X = XA_test[nonfire_wrong]
    # nonfire_wrong_X_second = XA_second[nonfire_wrong]
    # nonfire_wrong_X_fft = XA_fft[nonfire_wrong]
# ______

    wrong_X = np.array(wrong_X)
    wrong_X_second = np.array(wrong_X_second)
    wrong_X_fft = np.array(wrong_X_fft)

    print("Y_predict: ", y_predict.shape)
    print("wrong: ", wrong_X.shape, " fires: ", len(fire_wrong), " nonfire: ", len(nonfire_wrong))

    # if get_fires > 0 and get_nonfires == 0:
    get_fires = wrong_X.shape[0]
    fires_left, need_fires, added_fire, X_bal_fires, X_second_bal_fires, X_fft_bal_fires, actualfiresamples, actualhybridfire, actualfirefft = getBal(get_fires, wrong_X, actualfiresamples, actualhybridfire, actualfirefft)

    # fires_left, need_fires, added_fire, X_bal_fires, X_second_bal_fires, X_fft_bal_fires, actualfiresamples, actualhybridfire, actualfirefft = getBal(get_fires, nonfire_wrong_X, actualfiresamples, actualhybridfire, actualfirefft)
    print("fires left: ", fires_left)
    print("added fires: ", added_fire)
    print("Need fires: ", need_fires)
    print("should equal - fires: ", need_fires+added_fire)
    print("X_bal - fire: ", np.array(X_bal_fires).shape)
    # elif get_nonfires > 0 and get_fires == 0:
    #     nonfires_left, need_nonfires, added_nonfire, X_bal_nonfires, X_second_bal_nonfires, X_fft_bal_nonfires, nonfirebins, hybridnofirebins, fftnofirebins = getBal(get_nonfires, fire_wrong_X, nonfirebins, hybridnofirebins, fftnofirebins)
    #     print("nonfires left: ", nonfires_left)
    #     print("added nonfires: ", added_nonfire)
    #     print("Need nonfires: ", need_nonfires)
    #     print("should equal - nonfires: ", need_nonfires+added_nonfire)
    #     print("X_bal - nonfire: ", np.array(X_bal_nonfires).shape)
    # else:
    #     print("No need to balance")
    #     X, X_second, X_fft, y_train = shuffle(X, X_second, X_fft, y_train)
    #     fire_data = [actualfiresamples, actualhybridfire, actualfirefft] #actualhybridfirefft
    #     nonfire_data = [nonfirebins, hybridnofirebins, fftnofirebins]
    #     print("New X: ", X.shape)
    #     return X, X_second, X_fft, y_train, fire_data, nonfire_data


    # if get_fires > 0:
    X_bal_fires, X_second_bal_fires, X_fft_bal, actualfiresamples, actualhybridfire, actualfirefft, nonfire_wrong_X, nonfire_wrong_X_second, nonfire_wrong_X_fft = secondaryBal(need_fires, fires_left, actualfiresamples, actualhybridfire, actualfirefft, X_bal_fires, X_second_bal_fires, X_fft_bal_fires, wrong_X, wrong_X_second, wrong_X_fft)
    # X_bal_fires = np.concatenate((X_bal_fires, np.array(fire_wrong_X)), axis=0)
    # X_second_bal_fires = np.concatenate((X_second_bal_fires, np.array(fire_wrong_X_second)), axis=0)
    # X_fft_bal = np.concatenate((X_fft_bal, np.array(fire_wrong_X_second)), axis=0)
    X, X_second, X_fft, y_train = makeNewLabels(X_bal_fires, X_second_bal_fires, X_fft_bal_fires, nonfire_wrong_X, nonfire_wrong_X_second, nonfire_wrong_X_fft, X, X_second, X_fft, y_train, bal_mode="fire")
    # else:
    #     X_bal_nonfires, X_second_bal_nonfires, X_fft_bal_nonfires, nonfirebins, hybridnofirebins, fftnofirebins, fire_wrong_X, fire_wrong_X_second, fire_wrong_X_fft = secondaryBal(need_nonfires, nonfires_left, nonfirebins, hybridnofirebins, fftnofirebins, X_bal_nonfires, X_second_bal_nonfires, X_fft_bal_nonfires, fire_wrong_X, fire_wrong_X_second, fire_wrong_X_fft)
    #     X_bal_nonfires = np.concatenate((X_bal_nonfires, np.array(nonfire_wrong_X)), axis=0)
    #     X_second_bal_nonfires = np.concatenate((X_second_bal_nonfires, np.array(nonfire_wrong_X_second)), axis=0)
    #     X_fft_bal_nonfires = np.concatenate((X_fft_bal_nonfires, np.array(nonfire_wrong_X_fft)), axis=0)
    #     X, X_second, X_fft, y_train = makeNewLabels(X_bal_nonfires, X_second_bal_nonfires, X_fft_bal_nonfires, fire_wrong_X, fire_wrong_X_second, fire_wrong_X_fft, X, X_second, X_fft, y_train, bal_mode="nonfire")
# NOTE: new alfire is above comments, below are old


    # if need_fires > 0:
    #     while need_fires > 0 and fires_left > 0:
    #         for i, val in enumerate(actualfiresamples.keys()):
    #             if len(actualfiresamples[val]) > 0:
    #                 X_bal.append(actualfiresamples[val][0])
    #                 X_second_bal.append(actualhybridfire[val][0])
    #                 X_fft_bal.append(actualfirefft[val][0])
    #                 actualfiresamples[val] = actualfiresamples[val][1:]
    #                 actualhybridfire[val] = actualhybridfire[val][1:]
    #                 actualfirefft[val] = actualfirefft[val][1:]
    #                 need_fires-=1
    #             if need_fires == 0:
    #                 break
    #         fires_left = 0
    #         for i, val in enumerate(actualfiresamples.keys()):
    #             fires_left+=len(actualfiresamples[val])
    #         print("366 - fires left: ", fires_left)
    #
    # X_bal = np.array(X_bal)
    # X_second_bal = np.array(X_second_bal)
    # X_fft_bal = np.array(X_fft_bal)

    # print("X_bal: ", X_bal.shape)
    # if X_bal.shape[0] < wrong_X.shape[0]:
    #     wrong_X = wrong_X[:X_bal.shape[0]]
    #     wrong_X_second = wrong_X_second[:X_bal.shape[0]]
    #     wrong_X_fft = wrong_X_fft[:X_bal.shape[0]]
    #     print("new wrong shape: ", wrong_X.shape)

    # if X_bal.size > 2:
    #     fire_zeros = np.full((X_bal.shape[0], 1), 0)
    #     fire_ones = np.full((X_bal.shape[0], 1), 1)
    #     fire_labels = np.concatenate((fire_zeros, fire_ones), axis=1)
    #     nonfire_zeros = np.full((wrong_X.shape[0], 1), 0)
    #     nonfire_ones = np.full((wrong_X.shape[0], 1), 1)
    #     nonfire_labels = np.concatenate((nonfire_ones, nonfire_zeros), axis=1)
    #     X_new = np.concatenate((X_bal, wrong_X), axis=0)
    #     X_second_new = np.concatenate((X_second_bal, wrong_X_second), axis=0)
    #     X_fft_new = np.concatenate((X_fft_bal, wrong_X_fft), axis=0)
    #     y_new = np.concatenate((fire_labels, nonfire_labels), axis=0)
    #
    #     #concat with original dataset
    #     X = np.concatenate((X, X_new), axis=0)
    #     X_second = np.concatenate((X_second, X_second_new), axis=0)
    #     X_fft = np.concatenate((X_fft, X_fft_new), axis=0)
    #     y_train = np.concatenate((y_train, y_new), axis=0)

    X, X_second, X_fft, y_train = shuffle(X, X_second, X_fft, y_train)
    fire_data = [actualfiresamples, actualhybridfire, actualfirefft] #actualhybridfirefft
    nonfire_data = [nonfirebins, hybridnofirebins, fftnofirebins]
    print("New X: ", X.shape)
    return X, X_second, X_fft, y_train, fire_data, nonfire_data




def getFiresLeft(fire_data):
    result = []
    [result.extend(el) for el in fire_data.values()]
    return len(result)

def calibrateExperiments(experiments, leave_one_out):
    global globalmaxref
    global globalminref

    for e in experiments:
        to_keep = []
        sig_objects = {}
        if e.name == leave_one_out:
            e.leave_one_out = True
            if not e.fire:
                for i, o in enumerate(e.objects.keys()):
                    obj = e.objects[o]
                    if min(obj.reflection) < e.min_reflection:
                        e.min_reflection = min(obj.reflection)
                    if max(obj.reflection) > e.max_reflection:
                        e.max_reflection = max(obj.reflection)
                continue
            for k in range(3, max(e.fire_bin_list) + 37):
                sig_objects[k] = e.objects[k]
            e.objects = sig_objects
            # continue

        for i, o in enumerate(e.objects.keys()):
            obj = e.objects[o]
            if i > 2:
                if min(obj.reflection) < e.min_reflection:
                    e.min_reflection = min(obj.reflection)
                if max(obj.reflection) > e.max_reflection:
                    e.max_reflection = max(obj.reflection)
                if min(obj.reflection) < globalminref:
                    globalminref = min(obj.reflection)
                if max(obj.reflection) > globalmaxref:
                    globalmaxref = max(obj.reflection)
            try:
                if obj.significant or e.objects[o+1].significant or e.objects[o+2].significant or e.objects[o-1].significant or e.objects[o-2].significant:
                    to_keep.append(obj.distance)
            except:
                pass

        if e.leave_one_out:
            continue
        for k in to_keep:
            sig_objects[k] = e.objects[k]
        e.objects = sig_objects



def shuffleFormatDataLabels(fire_dataset, nonfire_dataset, second_fire_dataset, second_nonfire_dataset, fire_samples_fft=[], hybrid_fire_fft=[], new_nonfire_fft=[], new_hybrid_fft_nofire=[], leave_one_out=None):
    fire_zeros = np.full((fire_dataset.shape[0], 1), 0)
    fire_ones = np.full((fire_dataset.shape[0], 1), 1)
    fire_labels = np.concatenate((fire_zeros, fire_ones), axis=1)

    nonfire_zeros = np.full((nonfire_dataset.shape[0], 1), 0)
    nonfire_ones = np.full((nonfire_dataset.shape[0], 1), 1)
    nonfire_labels = np.concatenate((nonfire_ones, nonfire_zeros), axis=1)


    if fire_dataset.shape[0] > 0 and nonfire_dataset.shape[0] > 0:
        dataset = np.concatenate((nonfire_dataset, fire_dataset), axis=0)
        all_labels = np.concatenate((nonfire_labels, fire_labels), axis=0)
        second_dataset = np.concatenate((second_nonfire_dataset, second_fire_dataset), axis=0)
        dataset_fft = np.concatenate((new_nonfire_fft, fire_samples_fft), axis=0)
        second_dataset_fft = np.concatenate((new_hybrid_fft_nofire, hybrid_fire_fft), axis=0)
    elif fire_dataset.shape[0] > 0:
        dataset = fire_dataset
        all_labels = fire_labels
        second_dataset = second_fire_dataset
        dataset_fft = fire_samples_fft
        second_dataset_fft = hybrid_fire_fft
    else:
        dataset = nonfire_dataset
        all_labels = nonfire_labels
        second_dataset = second_nonfire_dataset
        dataset_fft = new_nonfire_fft
        second_dataset_fft = new_hybrid_fft_nofire

    if leave_one_out is None:
        try:
            X, y, X_second, X_fft, hybrid_X_fft = shuffle(dataset, all_labels, second_dataset, dataset_fft, second_dataset_fft)
        except:
            print("SHUFFLE DIDNT WORK")
            X = dataset
            y = all_labels
            X_second = second_dataset
            X_fft = dataset_fft
            hybrid_X_fft = second_dataset_fft

    else:
        X = dataset
        y = all_labels
        X_second = second_dataset
        X_fft = dataset_fft
        hybrid_X_fft = second_dataset_fft
    return X, y, X_second, X_fft, hybrid_X_fft

def formatData(fire_dataset, nonfire_dataset, leave_one_out_fire, leave_one_out_nofire, all_skipped_samples, hybrid, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft, fire_samples_fft, nonfire_samples_fft, hybrid_fire_fft, hybrid_nonfire_fft, test_experiments):
    leave_one_out_X, leave_one_out_y, second_loo_X, leave_one_out_X_fft, second_loo_X_fft = {}, {}, {}, {}, {}
    fire_data, nonfire_data = [], []
    if nonfire_dataset.size > 0:
        if hybrid:
            print("nonfire dataset is larger than 0 and hybrid")
            fire_dataset, hybrid_fire, fire_samples_fft, hybrid_fire_fft = shuffle(fire_dataset, hybrid_fire, fire_samples_fft, hybrid_fire_fft)
            nonfire_dataset, hybrid_nofire, nonfire_samples_fft, hybrid_nonfire_fft = shuffle(nonfire_dataset, hybrid_nofire, nonfire_samples_fft, hybrid_nonfire_fft)
        else:
            print("nonfire dataset is larger than 0 and NOT hybrid")
            fire_dataset, fire_samples_fft = shuffle(fire_dataset, fire_samples_fft)
            nonfire_dataset, nonfire_samples_fft = shuffle(nonfire_dataset, nonfire_samples_fft)

        firebins = {}
        actualfiresamples = {}
        actualhybridfire = {}
        actualfirefft = {}
        actualhybridfirefft = {}

        nonfirebins = {}
        flatfirebins = {}
        flatnonfirebins = {}
        hybridfirebins = {}
        hybridnofirebins = {}
        fftfirebins = {}
        fftnofirebins = {}
        hybridfftfirebins = {}
        hybridfftnofirebins = {}

        # distance_idx = 21 # len 9
        distance_idx = 13 # len 5
        # distance_idx = 5 # len 2

        for i, val in enumerate(fire_dataset):
            rounded = round(val[0][distance_idx], 3)
            if rounded not in firebins.keys():
                firebins[rounded] = 0
                actualfiresamples[rounded] = []
                actualhybridfire[rounded] = []
                actualfirefft[rounded] = []
                actualhybridfirefft[rounded] = []
            if rounded not in nonfirebins.keys():
                nonfirebins[rounded] = []
                flatnonfirebins[rounded] = []
                hybridnofirebins[rounded] = []
                fftnofirebins[rounded] = []
                hybridfftnofirebins[rounded] = []
        for i, val in enumerate(nonfire_dataset):
            rounded = round(val[0][distance_idx], 3)
            if rounded not in nonfirebins.keys():
                nonfirebins[rounded] = []
                flatnonfirebins[rounded] = []
                hybridnofirebins[rounded] = []
                fftnofirebins[rounded] = []
                hybridfftnofirebins[rounded] = []
        for i, val in enumerate(fire_dataset):
            # firebins[round(val[10], 3)]+=1 #appends the index in the dataset
            rounded = round(val[0][distance_idx], 3)
            firebins[rounded]+=1
            actualfiresamples[rounded].append(val)
            actualhybridfire[rounded].append(hybrid_fire[i])
            actualfirefft[rounded].append(fire_samples_fft[i])
            actualhybridfirefft[rounded].append(hybrid_fire_fft[i])
        for i, val in enumerate(nonfire_dataset):
            # nonfirebins[round(val[10], 3)].append(val)
            rounded = round(val[0][distance_idx], 3)
            if test_experiments == 0:
                if rounded in firebins.keys(): #taking this out because of second load in AL
                    nonfirebins[rounded].append(val)
                    hybridnofirebins[rounded].append(hybrid_nofire[i])
                    fftnofirebins[rounded].append(nonfire_samples_fft[i])
                    hybridfftnofirebins[rounded].append(hybrid_nonfire_fft[i])
            else:
                nonfirebins[rounded].append(val)
                hybridnofirebins[rounded].append(hybrid_nofire[i])
                fftnofirebins[rounded].append(nonfire_samples_fft[i])
                hybridfftnofirebins[rounded].append(hybrid_nonfire_fft[i])


        for i, val in enumerate(firebins.keys()):
            actualfiresamples[val], actualhybridfire[val], actualfirefft[val], actualhybridfirefft[val] = shuffle(actualfiresamples[val], actualhybridfire[val], actualfirefft[val], actualhybridfirefft[val])
            nonfirebins[val], hybridnofirebins[val], fftnofirebins[val], hybridfftnofirebins[val] = shuffle(nonfirebins[val], hybridnofirebins[val], fftnofirebins[val], hybridfftnofirebins[val])
            print("count: ", i, " fire: ", firebins[val], " nonfire: ", len(nonfirebins[val]))

        fire_data = [actualfiresamples, actualhybridfire, actualfirefft] #actualhybridfirefft
        nonfire_data = [nonfirebins, hybridnofirebins, fftnofirebins] #hybridfftnofirebinss

    else:
        X, y, X_second, X_fft, X_second_fft = np.array([]), np.array([]), np.array([]), np.array([]), np.array([])

    if len(leave_one_out_nofire.keys()) is not 0:
        for dist in leave_one_out_nofire.keys():
            if hybrid:
                test_x, test_y, second_x_test, test_x_fft, second_x_test_fft = shuffleFormatDataLabels(leave_one_out_fire[dist], leave_one_out_nofire[dist], hybrid_loo_fire[dist], hybrid_loo_nofire[dist], fire_samples_fft=fire_samples_loo_fft[dist], hybrid_fire_fft=hybrid_fire_loo_fft[dist], new_nonfire_fft=nonfire_samples_loo_fft[dist], new_hybrid_fft_nofire=hybrid_nonfire_loo_fft[dist], leave_one_out="leave one out")
            second_loo_X[dist] = second_x_test
            leave_one_out_X[dist] = test_x
            leave_one_out_y[dist] = test_y
            leave_one_out_X_fft[dist] = test_x_fft
            second_loo_X_fft[dist] = second_x_test_fft

    return fire_data, nonfire_data, leave_one_out_X, leave_one_out_y, second_loo_X, leave_one_out_X_fft, second_loo_X_fft, all_skipped_samples

def nonParallelLeaveOneOutSampleCreation(obj, exp, hybrid):
    start_time = 0
    end_time = obj.len_reflection
    leave_one_out_fire, leave_one_out_nofire, skipped_samples, hybrid_loo_fire, hybrid_loo_nofire = sample_factory.makeSamples(exp, obj, start_time, end_time, flat, leave_one_out_bool=True, hybrid=hybrid)

    return {"distance": obj.distance,
            "samples": [np.array(leave_one_out_fire), np.array(leave_one_out_nofire), skipped_samples, np.array(hybrid_loo_fire), np.array(hybrid_loo_nofire)]}

def parallelLeaveOneOutSampleCreation(pass_arr):
    obj = pass_arr[0]
    exp = pass_arr[1]
    start_time = pass_arr[2]
    end_time = pass_arr[3]
    hybrid = pass_arr[4]

    leave_one_out_fire, leave_one_out_nofire, skipped_samples, hybrid_loo_fire, hybrid_loo_nofire = sample_factory.makeSamples(exp, obj, start_time, end_time, leave_one_out_bool=True, hybrid=hybrid)

    return {"distance": obj.distance,
            "samples": [np.array(leave_one_out_fire), np.array(leave_one_out_nofire), skipped_samples, np.array(hybrid_loo_fire), np.array(hybrid_loo_nofire)]}

#could try to parallelize, but needs to omake this sequential so that the distances are all consistent
def makeLeaveOneOutSamples(exp, hybrid):
    fire_samples, nonfire_samples, leave_one_out_samples_fire, leave_one_out_samples_nofire, fires, nonfires, leave_one_out_samples, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire = [], [], [], [], [], [], [], [], [], [], [], [], []
    all_skipped_samples = []

    fire_samples_dict, nonfire_samples_dict, all_skipped_samples_dict, hybrid_fire_dict, hybrid_nofire_dict = {}, {}, {}, {}, {}

    cores = 40
    chunksize = 1

    pass_arr = []
    for o in exp.objects.keys():
        obj = exp.objects[o]

        #filters
        if o < (list(exp.objects.keys())[:1][0] + 2) or obj.distance > (list(exp.objects.keys())[-1:][0] - 2):
            continue
        if obj.distance > 170 and not exp.fire: #changed from 175
            continue
        if exp.fire and obj.distance > (exp.fire_bin_list[-1:][0] + 37): # THIS LIMITS THE TESTING SEARCH SPACE TO JUST BEHIND THE FIRE
            continue

        fire_samples_dict[obj.distance] = []
        nonfire_samples_dict[obj.distance] = []
        all_skipped_samples_dict[obj.distance] = []
        hybrid_fire_dict[obj.distance] = []
        hybrid_nofire_dict[obj.distance] = []

        # This is assuming that fire experiments will never be longer than 1 hour
        obj = exp.objects[o]
        start_time = 0
        end_time = 40000
        step = 40000
        loops_per_dist = 0
        while end_time <= obj.len_reflection:
            pass_arr.append([obj, exp, start_time, end_time, hybrid])
            start_time+=step
            end_time+=step
            loops_per_dist+=1
            # print('samples: ', )
        if loops_per_dist != 1:
            pass_arr.append([obj, exp, start_time, obj.len_reflection, hybrid])
            loops_per_dist+=1

    with Pool(processes=cores) as pool:
        sample_results = pool.map(parallelLeaveOneOutSampleCreation, pass_arr, chunksize)

    for i, r in enumerate(sample_results):
        fire_samples_dict[r["distance"]].append(np.array(r["samples"][0]))
        nonfire_samples_dict[r["distance"]].append(np.array(r["samples"][1]))
        all_skipped_samples_dict[r["distance"]] = all_skipped_samples_dict[r["distance"]] + r["samples"][2]
        hybrid_fire_dict[r["distance"]].append(np.array(r["samples"][3]))
        hybrid_nofire_dict[r["distance"]].append(np.array(r["samples"][4]))
    all_skipped_samples = all_skipped_samples_dict

    for i, val in enumerate(fire_samples_dict.keys()):
        fire_size = False
        nonfire_size = False
        for x in fire_samples_dict[val]:
            if x.size > 0:
                fire_size = True
                break
        if fire_size:
            fire_samples_dict[val] = np.concatenate([x for x in fire_samples_dict[val] if x.size > 0])
            hybrid_fire_dict[val] = np.concatenate([x for x in hybrid_fire_dict[val] if x.size > 0])
        else:
            fire_samples_dict[val] = fire_samples_dict[val][0]
            hybrid_fire_dict[val] = hybrid_fire_dict[val][0]

        for x in nonfire_samples_dict[val]:
            if x.size > 0:
                nonfire_size = True
                break
        if nonfire_size:
            nonfire_samples_dict[val] = np.concatenate([x for x in nonfire_samples_dict[val] if x.size > 0])
            hybrid_nofire_dict[val] = np.concatenate([x for x in hybrid_nofire_dict[val] if x.size > 0])
        else:
            nonfire_samples_dict[val] = nonfire_samples_dict[val][0]
            hybrid_nofire_dict[val] = hybrid_nofire_dict[val][0]

    fire_samples_fft, hybrid_fire_fft, fire_samples, hybrid_fire = sortSamples_dict(fire_samples_dict, hybrid_fire_dict, exp, loops_per_dist)
    nonfire_samples_fft, hybrid_nonfire_fft, nonfire_samples, hybrid_nofire = sortSamples_dict(nonfire_samples_dict, hybrid_nofire_dict, exp, loops_per_dist)

    skipped = []
    current_skipped = []

    return fire_samples, nonfire_samples, all_skipped_samples, hybrid_fire, hybrid_nofire, fire_samples_fft, hybrid_fire_fft, nonfire_samples_fft, hybrid_nonfire_fft

def sortSamples_dict(main_samples, hybrid_samples, exp, loops_per_dist):
    samples_fft = dict.fromkeys(main_samples.keys(), np.array([]))
    hybrid_fft = dict.fromkeys(main_samples.keys(), np.array([]))
    samples3d = dict.fromkeys(main_samples.keys(), np.array([]))
    samples3d_hybrid = dict.fromkeys(main_samples.keys(), np.array([]))

    distance = 0
    for i, dist in enumerate(main_samples.keys()):
        single_sample_fft = normalizing.normalizeFFT(main_samples[dist])
        single_hybrid_fft = normalizing.normalizeFFT(hybrid_samples[dist])
        single_sample = normalizing.normalize3dScenario(main_samples[dist], exp, globalmaxref, globalminref)
        single_hybrid = normalizing.normalize3dScenario(hybrid_samples[dist], exp, globalmaxref, globalminref)

        # print("dist: ", distance, " shape: ", single_sample.shape)

        samples_fft[dist] = single_sample_fft
        hybrid_fft[dist] = single_hybrid_fft
        samples3d[dist] = single_sample
        samples3d_hybrid[dist] = single_hybrid

    return samples_fft, hybrid_fft, samples3d, samples3d_hybrid




def sortSamples(main_samples, hybrid_samples, exp, loops_per_dist):
    flat_samples = [None] * (len(main_samples)//loops_per_dist)
    samples_fft = [None] * (len(main_samples)//loops_per_dist)
    hybrid_fft = [None] * (len(main_samples)//loops_per_dist)
    samples3d = [None] * (len(main_samples)//loops_per_dist)
    samples3d_hybrid = [None] * (len(main_samples)//loops_per_dist)

    distance = 0
    for i, sample in enumerate(main_samples):
        if i > 0 and i%loops_per_dist == 0:
            distance+=1

        flat_sample = sample_factory.makeFlatFrom3D(sample, exp)
        flat_sample = normalizing.normalize1dScenario(flat_sample, exp)

        single_sample_fft = normalizing.normalizeFFT(sample)
        single_hybrid_fft = normalizing.normalizeFFT(hybrid_samples[i])

        single_sample = normalizing.normalize3dScenario(sample, exp, globalmaxref, globalminref)
        single_hybrid = normalizing.normalize3dScenario(hybrid_samples[i], exp, globalmaxref, globalminref)

        # print("dist: ", distance, " shape: ", single_sample.shape)

        # print("before if: ", flat_samples[distance])
        if flat_sample.size > 2:
            if flat_samples[distance] is None:
                # print('flat_sample: ', flat_sample)
                flat_samples[distance] = flat_sample
                samples_fft[distance] = single_sample_fft
                hybrid_fft[distance] = single_hybrid_fft
                samples3d[distance] = single_sample
                samples3d_hybrid[distance] = single_hybrid
            else:
                # print(flat_samples[distance].shape, " and ", flat_sample.shape)
                flat_samples[distance] = np.concatenate((flat_samples[distance], flat_sample), axis=0)
                samples_fft[distance] = np.concatenate((samples_fft[distance], single_sample_fft), axis=0)
                hybrid_fft[distance] = np.concatenate((hybrid_fft[distance], single_hybrid_fft), axis=0)
                samples3d[distance] = np.concatenate((samples3d[distance], single_sample), axis=0)
                samples3d_hybrid[distance] = np.concatenate((samples3d_hybrid[distance], single_hybrid), axis=0)
                # print(samples3d[distance].shape)
                # print('Concat')

        # if i > 0 and i%loops_per_dist == (loops_per_dist - 1):
    for i in range(len(flat_samples)):
        if flat_samples[i] is None:
            flat_samples[i] = np.array([])
            samples_fft[i] = np.array([])
            hybrid_fft[i] = np.array([])
            samples3d[i] = np.array([])
            samples3d_hybrid[i] = np.array([])

    return flat_samples, samples_fft, hybrid_fft, samples3d, samples3d_hybrid


def setupParallel(experiments, leave_one_out, model, hybrid):
    global bin_count

    fire_samples, nonfire_samples, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire = [], [], [], [], [], [], [], []
    all_fire_fft, all_nonfire_fft, all_hybrid_fire_fft, all_hybrid_nonfire_fft = np.array([]), np.array([]), np.array([]), np.array([])
    all_fire_samples, all_nonfire_samples, all_hybrid_fire_samples, all_hybrid_nofire_samples = np.array([]), np.array([]), np.array([]), np.array([])
    max_fft_fire = 0
    max_fft_nofire = 0
    total_fft_fire = 0
    total_fft_nofire = 0
    num_fft_fire = 0
    num_fft_nofire = 0
    total_firemax_idx = 0
    total_nofiremax_idx = 0

    cores = 40
    chunksize = 1 #8

    if model is None:
        for exp in experiments:
            fire_samples, nonfire_samples = [], []
            hybrid_fire, hybrid_nofire = [], []
            if exp.name == leave_one_out:
                leave_one_out_exp = exp
                continue

            print("Creating Samples for: ", exp)
            try:
                obj_dists = list(exp.objects.keys())
                pass_arr = []
                for dist in obj_dists:
                    pass_arr.append([dist, leave_one_out, hybrid])
                with Pool(processes=cores) as pool:
                    exp_results = pool.map(exp.parallelObjectSampleCreation, pass_arr, chunksize)
            except:
                print("Experiment too big to use parallel")
                for dist in obj_dists:
                    cnn = True
                    svm = False
                    flat = False
                    exp_results.append(exp.nonParallelObjectSampleCreation(dist, cnn, leave_one_out, svm, flat, hybrid))

            for idx, r in enumerate(exp_results):
                fire_samples = fire_samples + r[0]
                nonfire_samples = nonfire_samples + r[1]
                if hybrid:
                    hybrid_fire = hybrid_fire + r[4]
                    hybrid_nofire = hybrid_nofire + r[5]

            if exp.name != leave_one_out:
                fire_samples = np.array(fire_samples)
                nonfire_samples = np.array(nonfire_samples)
                if hybrid:
                    hybrid_fire = np.array(hybrid_fire)
                    hybrid_nofire = np.array(hybrid_nofire)

            if nonfire_samples.size > 1 or fire_samples.size > 1:
                if nonfire_samples.size > 1:
                    nonfire_samples_fft = normalizing.normalizeFFT(nonfire_samples)
                    nonfire_samples = normalizing.normalize3dScenario(nonfire_samples, exp)
                    if hybrid:
                        hybrid_nonfire_samples_fft = normalizing.normalizeFFT(hybrid_nofire)
                        hybrid_nofire = normalizing.normalize3dScenario(hybrid_nofire, exp)
                        if all_hybrid_nofire_samples.size < 2 and all_nonfire_samples.size < 2:
                            all_nonfire_samples = nonfire_samples
                            all_hybrid_nofire_samples = hybrid_nofire
                            all_nonfire_fft = nonfire_samples_fft
                            all_hybrid_nonfire_fft = hybrid_nonfire_samples_fft
                        else:
                            all_nonfire_samples = np.concatenate((all_nonfire_samples, nonfire_samples), axis=0)
                            all_hybrid_nofire_samples = np.concatenate((all_hybrid_nofire_samples, hybrid_nofire), axis=0)
                            all_nonfire_fft = np.concatenate((all_nonfire_fft, nonfire_samples_fft), axis=0)
                            all_hybrid_nonfire_fft = np.concatenate((all_hybrid_nonfire_fft, hybrid_nonfire_samples_fft), axis=0)
                    else:
                        if all_nonfire_samples.size < 2:
                            all_nonfire_samples = nonfire_samples
                            all_nonfire_fft = nonfire_samples_fft
                        else:
                            all_nonfire_samples = np.concatenate((all_nonfire_samples, nonfire_samples), axis=0)
                            all_nonfire_fft = np.concatenate((all_nonfire_fft, nonfire_samples_fft), axis=0)
                if fire_samples.size > 1:
                    #normalize the 3d version of those samples
                    fire_samples_fft = normalizing.normalizeFFT(fire_samples)
                    fire_samples = normalizing.normalize3dScenario(fire_samples, exp)
                    if hybrid:
                        hybrid_fire_samples_fft = normalizing.normalizeFFT(hybrid_fire)
                        hybrid_fire = normalizing.normalize3dScenario(hybrid_fire, exp)
                        if all_hybrid_fire_samples.size < 2 and all_fire_samples.size < 2:
                            all_fire_samples = fire_samples
                            all_hybrid_fire_samples = hybrid_fire
                            all_fire_fft = fire_samples_fft
                            all_hybrid_fire_fft = hybrid_fire_samples_fft
                        else:
                            if hybrid_fire.size > 2 and fire_samples.size > 2:
                                all_fire_samples = np.concatenate((all_fire_samples, fire_samples), axis=0)
                                all_hybrid_fire_samples = np.concatenate((all_hybrid_fire_samples, hybrid_fire), axis=0)
                                all_fire_fft = np.concatenate((all_fire_fft, fire_samples_fft), axis=0)
                                all_hybrid_fire_fft = np.concatenate((all_hybrid_fire_fft, hybrid_fire_samples_fft), axis=0)
                    else:
                        if all_fire_samples.size < 2:
                            all_fire_samples = fire_samples
                            all_fire_fft = fire_samples_fft
                        else:
                            all_fire_samples = np.concatenate((all_fire_samples, fire_samples), axis=0)
                            all_fire_fft = np.concatenate((all_fire_fft, fire_samples_fft), axis=0)
            else:
                print("Samples: ", nonfire_samples)
                continue
    else:
        leave_one_out_exp = experiments[0]

    if leave_one_out is not None:
        #doing leave one out last
        leave_one_out_fire, leave_one_out_nofire, all_skipped_samples, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft = leaveOneOutSamplesMidstep(leave_one_out_exp, fire_samples, nonfire_samples, hybrid)
    else:
        all_skipped_samples = []

    return all_fire_samples, all_nonfire_samples, leave_one_out_fire, leave_one_out_nofire, all_skipped_samples, all_hybrid_fire_samples, all_hybrid_nofire_samples, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft, all_fire_fft, all_nonfire_fft, all_hybrid_fire_fft, all_hybrid_nonfire_fft

def leaveOneOutSamplesMidstep(leave_one_out_exp, fire_samples, nonfire_samples, hybrid):
    if hybrid:
        print("HYBRID - Creating Leave One Out Samples for: ", leave_one_out_exp)
    else:
        print("Creating Leave One Out Samples for: ", leave_one_out_exp)
    leave_one_out_fire, leave_one_out_nofire, all_skipped_samples, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft = makeLeaveOneOutSamples(leave_one_out_exp, hybrid)

    return leave_one_out_fire, leave_one_out_nofire, all_skipped_samples, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft

def makeDataset(experiments, test_experiments, leave_one_out=None, model=None, hybrid=False):
    fire_samples, nonfire_samples, leave_one_out_samples_fire, leave_one_out_samples_nofire, fires, nonfires, leave_one_out_samples, leave_one_out_fire, leave_one_out_nofire = [], [], [], [], [], [], [], [], []
    print("Experiments: ", len(experiments))
    fire_samples, nonfire_samples, leave_one_out_fire, leave_one_out_nofire, all_skipped_samples, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft, fire_samples_fft, nonfire_samples_fft, hybrid_fire_fft, hybrid_nonfire_fft = setupParallel(experiments, leave_one_out, model, hybrid)

    print("Fire Samples:         ", fire_samples.shape)
    print("Nonfire Samples:      ", nonfire_samples.shape)
    print("HYBRID Fire Samples:         ", hybrid_fire.shape)
    print("HYBRID Nonfire Samples:      ", hybrid_nofire.shape)


    if leave_one_out is not None:
        # leave_one_out_samples = np.concatenate((leave_one_out_samples_nofire, leave_one_out_samples_fire), axis=0)
        print("Leave one out no fire: ", len(leave_one_out_nofire[list(leave_one_out_nofire.keys())[0]]))
        print("Leave one out fire:    ", len(leave_one_out_fire[list(leave_one_out_fire.keys())[0]]))
        # print("Leave one out no fire samples: ", leave_one_out_nofire[0].shape)
        try:
            print("leave one out fire above")
        except:
            split_scen = leave_one_out.split('_')
            if 'fire' not in split_scen:
                print("Leave one out nofire samples:    ", leave_one_out_nofire[len(leave_one_out_nofire) - 1].shape)
            else:
                print("Leave one out fire samples:    ", leave_one_out_fire[len(leave_one_out_fire) - 1].shape)

    return formatData(fire_samples, nonfire_samples, leave_one_out_fire, leave_one_out_nofire, all_skipped_samples, hybrid, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft, fire_samples_fft, nonfire_samples_fft, hybrid_fire_fft, hybrid_nonfire_fft, test_experiments)
