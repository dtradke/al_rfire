# RFire
# 05/21/20
#
# 1DCNN model for the second RFire algorithm, using a CNN. Good results. Models have been trained already using temporal samples.


# cnn model
import tensorflow as tf
from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
from matplotlib import pyplot
from keras.models import Sequential, Model
from keras.layers import Dense, GlobalAveragePooling1D, Bidirectional, TimeDistributed, LSTM, Concatenate, Reshape
from keras.layers import Flatten, Input
from keras.layers import Dropout
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.utils import to_categorical
from keras.models import load_model
from keras.optimizers import SGD
from sklearn.utils import shuffle
import time
import process_dat
import sys
import pickle
import util
import model



class FinalResult(object):

    def __init__(self, fire_distance=257, latency=10000, false_alarms=[], TP=0, TN=0, FP=0, FN=0):
        self.fire_distance = fire_distance
        self.latency = latency
        self.false_alarms = false_alarms
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def __repr__(self):
        return "FinalResult(distance: {}, latency: {}, false alarms: {}, TP: {}, TN: {}, FP: {}, FN: {})".format(self.fire_distance, self.latency, self.false_alarms, self.TP, self.TN, self.FP, self.FN)

class Result(object):

    def __init__(self, distance=0, fire=False, latency=10000, false_alarms=[], predicted_fire_time=10000, TP=0, TN=0, FP=0, FN=0):
        self.distance = distance
        self.fire = fire
        self.latency = latency
        self.false_alarms = false_alarms
        self.predicted_fire_time = predicted_fire_time
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def __repr__(self):
        return "Result(distance: {}, latency: {}, false alarms: {}, predicted_fire_time: {}, TP: {}, TN: {}, FP: {}, FN: {})".format(self.distance, self.latency, self.false_alarms, self.predicted_fire_time, self.TP, self.TN, self.FP, self.FN)

class Alarm(object):

    def __init__(self, distance=0, time=10000, fire=False, latency=10000):
        self.distance = distance
        self.time = time
        self.fire = fire
        self.latency = latency

    def __repr__(self):
        if self.fire:
            return "Alarm(distance: {}, time: {}, fire: {}, latency: {})".format(self.distance, self.time, self.fire, self.latency)
        else:
            return "Alarm(distance: {}, time: {}, fire: {})".format(self.distance, self.time, self.fire)

class RealAlarm(object):

    def __init__(self, distance=0, time=10000, fire=False, latency=10000):
        self.distance = distance
        self.time = time
        self.fire = fire
        self.latency = latency

    def __repr__(self):
        if self.fire:
            return "RealAlarm(distance: {}, time: {}, fire: {}, latency: {})".format(self.distance, self.time, self.fire, self.latency)
        else:
            return "RealAlarm(distance: {}, time: {}, fire: {})".format(self.distance, self.time, self.fire)


def getNewInput(train_t):
    bm2_idx = 0
    bm1_idx = 5
    reflections_idx = 10
    bp1 = 15
    bp2 = 20
    train_bm2 = train_t[:,:,bm2_idx]
    train_bm1 = train_t[:,:,bm1_idx]
    train_t_ref = train_t[:,:,reflections_idx]
    train_bp1 = train_t[:,:,bp1]
    train_bp2 = train_t[:,:,bp2]

    arr = []
    for i, val in enumerate(train_t_ref):
        arr.append(np.transpose(np.array([train_bm2[i], train_bm1[i], val, train_bp1[i], train_bp2[i]])))

    return np.array(arr)

# __________

# CNN LSTM hybrid
def train_model(trainX, trainXsecond, X_fft, trainy, save=False, epochs=1, batch_size=32):
    if save:
        verbose = 1
    else:
        verbose = 0
    epochs = epochs
    n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], trainy.shape[1]

    print('In training model')
    mod = model.HybridModel(trainX, trainy, X_fft, epochs, batch_size)
    trainX, trainXsecond, trainy, X_fft = shuffle(trainX, trainXsecond, trainy, X_fft)
    mod.fit(trainX, trainXsecond, trainy, X_fft, epochs, batch_size, verbose, save)
    return mod


def findIfFire(y_ground):
    for i in y_ground:
        if i[0] < i[1]:
            return True
    return False

def TimeSeriesTest(y_test, y_pred, leave_one_out, min_ignition_sample, all_skipped_samples):
    results = {}
    # min_ignition_sample = 10000
    closest_distance = 0
    alarms = []

# TAKE OUT WHEN CLASSIFICATION ISNT AT THE BEGINNING
    if min_ignition_sample < 10000:
        min_ignition_sample = min_ignition_sample - 30

    print("Length of y_test: ", len(y_test.keys()))
    print("Length of y_pred: ", len(y_pred.keys()))

    for idx, y_test_key in enumerate(y_test.keys()):
        y_ground = y_test[y_test_key]
        is_fire = findIfFire(y_ground)
        correct = 0
        incorrect = 0
        FN = 0
        FP = 0
        TP = 0
        TN = 0
        FN_before_fire = 0
        fire_flag = False
        early_classification = 0
        false_alarms = []
        real_false_alarm = 0
        ones_in_a_row = 0
        latency = 10000
        failed_detection = []
        predicted_fire_time = 10000
        pred_made = 0

        if y_pred[y_test_key].size < 2:
            continue

        # print("Skipped: ", all_skipped_samples[y_test_key])

        i = 0
        timestep = 0
        # for i, val in enumerate(y_ground): #actual values
        while pred_made < y_ground.shape[0]:
            val = y_ground[i]
            timestep+=1

            if timestep in all_skipped_samples[y_test_key]:
                ones_in_a_row = 0
                # print("Skipping timestep: ", timestep)
                continue

            pred_made+=1
            print(timestep, ' i: ', i, ' prediction: ', y_pred[y_test_key][i], ' ground: ', val)


            if y_pred[y_test_key][i][1] > 0.5: #was 0.6
                ones_in_a_row += 1
                if ones_in_a_row > 2:
                    predicted_fire_time = timestep+10 #i
                    ones_in_a_row = 0
                    try:
                        if val[0] < val[1]:# or y_ground[i+5][0] < y_ground[i+5][1]:#abs(min_ignition_sample - i) <= 5: #if it actually is a fire
                            fire_flag = True
                            latency = (timestep - min_ignition_sample)
                            print("==== KNOWN FIRE BIN ====")
                            print("distance: ", y_test_key)
                            print("time: ", timestep)
                            # print("ignition_sample: ", min_ignition_sample)
                            print("Latency: ", latency, " second(s)")
                            alarms.append(Alarm(y_test_key, timestep, True, latency))
                        else: #fire detected but not a ground truth classified fire sample yet
                            if timestep < min_ignition_sample:
                                print("==== False Alarm at time ", timestep, " ====")
                                alarms.append(Alarm(y_test_key, timestep, False, 10000))
                                false_alarms.append(timestep)
                            else:
                                fire_flag = True
                                latency = (timestep - min_ignition_sample)
                                print("==== PREDICTED FIRE ====")
                                print("distance: ", y_test_key)
                                print("time: ", timestep)
                                # print("ignition_sample: ", min_ignition_sample)
                                print("Latency: ", latency, " second(s)")
                                alarms.append(Alarm(y_test_key, timestep, True, latency))
                    except:
                        print("---there was an error here---")
                        pass
                if val[0] < val[1]: #actual fire
                    correct += 1
                    TP += 1
                else: # actual no fire
                    incorrect += 1
                    FP += 1
            else: #not predicted fire
                ones_in_a_row = 0
                if val[0] > val[1]: #actual no fire
                    correct += 1
                    TN += 1
                else: #actual fire
                    incorrect += 1
                    FN += 1
                    # failed_detection.append(i)
                    if not fire_flag:
                        FN_before_fire += 1
            i+=1

        print("Distance: ", y_test_key)
        print("False Alarms: ", false_alarms)

        results[idx] = Result(distance=y_test_key, fire=is_fire, latency=latency, false_alarms=false_alarms, predicted_fire_time=predicted_fire_time, TP=TP, TN=TN, FP=FP, FN=FN)

    min_fire_distance = 257
    min_latency = 10000
    alarm_count = {}
    for i in range(256):
        alarm_count[i] = []

    for i, a in enumerate(alarms):
        print(a)
        alarm_count[a.distance].append(a.time)
        if a.distance < min_fire_distance and a.time >= min_ignition_sample:
            min_fire_distance = a.distance
        if a.latency < min_latency and a.time >= min_ignition_sample:
            min_latency = a.latency


    # latencies = []
    false_alarm_list = []

    for idx, r in enumerate(results.keys()):
        false_alarm_list = false_alarm_list + results[r].false_alarms

# _____ below for cs848 final
    all_res_TP = 0
    all_res_FP = 0
    all_res_FN = 0
    all_res_TN = 0
    for r in results.keys():
        all_res_TP+=results[r].TP
        all_res_FP+=results[r].FP
        all_res_FN+=results[r].FN
        all_res_TN+=results[r].TN

    print("All STATS: ")
    print("TP: ", all_res_TP)
    print("FP: ", all_res_FP)
    print("FN: ", all_res_FN)
    print("TN: ", all_res_TN)
    try:
        prec = all_res_TP / (all_res_TP + all_res_FP)
    except:
        prec = 0
    try:
        recall = all_res_TP / (all_res_TP + all_res_FN)
    except:
        recall = 0
    try:
        f1_score = 2 * ((prec * recall) / (prec + recall))
    except:
        f1_score = 0
    print("Precision: ", prec)
    print("Recall: ", recall)
    print("F1-score: ", f1_score)
# _______ above is for cs848 final


    for r in results.keys():
        if results[r].fire:
            closest_known_fire = results[r]
            break

    my_set = set(false_alarm_list)
    unique_false_alarms = list(my_set)

    unique_false_alarms = lessThanIgnition(unique_false_alarms, min_ignition_sample)

    print("Fire starts at: ", min_ignition_sample)
    print("Fire detected at bin: ", min_fire_distance)
    print("Distance (meters):    ", min_fire_distance*.044)
    print("Fire latency:         ", min_latency)
    print("Unique false alarms:  ", unique_false_alarms)


    real_fire_alarms = []
    # print("alarm count: ", alarm_count)
    for dist in alarm_count.keys():
        fire = False
        if dist < 2 or dist > 200:
            continue
        cur_dist = alarm_count[dist]
        closer_list = alarm_count[dist-1]
        further_list = alarm_count[dist+1]
        for time in cur_dist:
            # print("cur_dist: ", cur_dist)
            # print("closer_list: ", closer_list)
            closer = [abs(x - time) for x in closer_list]
            # closer = map(lambda x: (abs(x - time)), closer_list)
            # print('closer: ', closer)
            # print("Further list: ", further_list)
            further = [abs(y - time) for y in further_list]
            # further = map(lambda y: (abs(y - time)), further_list)
            # print("further", further)
            # total_list = closer + further
            # print('total_list: ', total_list)
            # count = len([i for i in total_list if i < 3])
            closer_count = len([i for i in closer if i < 6])
            further_count = len([i for i in further if i < 6])
            # print("count: ", count)
            # if count > 0:
            if closer_count > 0 and further_count > 0:
            # if (0 in closer and 0 in further) or (1 in closer and 1 in further) or (1 in closer and 0 in further) or (0 in closer and 1 in further)or (2 in closer and 0 in further) or (0 in closer and 2 in further) or (2 in closer and 1 in further) or (1 in closer and 2 in further):
                if time >= min_ignition_sample:
                    fire = True
                    latency = time - min_ignition_sample
                    # print("fire: ", fire, " latency: ", latency)
                else:
                    fire = False
                    latency = 10000
                real_fire_alarms.append(RealAlarm(dist, time, fire, latency))


    final_result = FinalResult(min_fire_distance, min_latency, unique_false_alarms)
    return final_result, real_fire_alarms

def lessThanIgnition(false_alarms, ignition):
    real_false_alarms = []
    for i in false_alarms:
        if i < ignition:
            real_false_alarms.append(i)
    return real_false_alarms


def getIgnitionSample(leave_one_out, y_test):
    ignitions = []
    if leave_one_out is not None:
        print("LEAVE ONE OUT: ", leave_one_out)
        for i, val in enumerate(y_test):
            for j, value in enumerate(val):
                if value[0] < value[1]:
                    ignitions.append(j)
                    break
            else:
                ignitions.append(1000000)
        return ignitions
    else:
        return 0



def getModel(leave_one_out, X, X_second, X_fft, y_train, model_str, save=False):
    if model_str is None:
        epochs = 25 #for RFIRE
        print("Training with: ", X.shape, " and ", X_second.shape)
        mod = train_model(X, X_second, X_fft, y_train, save=save, epochs=epochs)
        if save:
            time_string = time.strftime("%Y%m%d-%H%M%S")
            fname = 'AL_models/AL_rfire-' + time_string + '_without-' + leave_one_out + '-fires_'+ str(X.shape[0]) +'.h5'
            #
            try:
                mod.save_weights(fname)
                print("Saving: ", fname)
            except:
                print("Not saving model")
                pass
    else:
        # fname = 'AL_models/' + model_str
        # model=load_model(fname)
        print("Loading: ", model_str)
        print("Testing with: ", X.shape, " and ", X_second.shape, " fft: ", X_fft.shape, " y: ", y_train.shape)
        mod = model.HybridModel(X, y_train, X_fft, weightsFileName=model_str)

    return mod


# run an experiment
def run_experiment(leave_one_out, model=None, repeats=1):
    full_TP = 0
    full_TN = 0
    full_FP = 0
    full_FN = 0
    total_latency = 0
    total_false_alarms = 0
    total_distance = 0
    test_experiments = 0
    for a in range(repeats):
        if "load_data" in sys.argv:
            print("loading data")
            fire_data, nonfire_data, X_test, y_test, X_test_second, leave_one_out_X_fft, second_loo_X_fft, leave_one_out_fires, all_skipped_samples, ignition_sample = process_dat.getRangeProfileSamples(leave_one_out=leave_one_out, model=model, hybrid=True, test_experiments=test_experiments)
            model = None
            X = np.load("X.npy")
            X_second = np.load("X_second.npy")
            X_fft = np.load("X_fft.npy")
            y_train = np.load("y_train.npy")
        else:
            if model is None and test_experiments == 0:
                fire_data, nonfire_data, X_test, y_test, X_test_second, leave_one_out_X_fft, second_loo_X_fft, leave_one_out_fires, all_skipped_samples, ignition_sample = process_dat.getRangeProfileSamples(leave_one_out=leave_one_out, hybrid=True, test_experiments=test_experiments)
                fires_left = util.getFiresLeft(fire_data[0])
            elif model is None and test_experiments > 0:
                fire_data, nonfire_data, X_test, y_test, X_test_second, _, _, _, _, _ = process_dat.getRangeProfileSamples(leave_one_out=leave_one_out, hybrid=True, test_experiments=test_experiments)
                fires_left = util.getFiresLeft(fire_data[0])
            else:
                fire_data, nonfire_data, X_test, y_test, X_test_second, leave_one_out_X_fft, second_loo_X_fft, leave_one_out_fires, all_skipped_samples, ignition_sample = process_dat.getRangeProfileSamples(leave_one_out=leave_one_out, model=model, hybrid=True, test_experiments=test_experiments)
                keys = list(X_test.keys())
                for k in keys:
                    if X_test[k].size > 0:
                        key = k
                        break

                X, X_second, X_fft, y_train = X_test[key], X_test_second[key], leave_one_out_X_fft[key], y_test[key]
                fires_left = 0
                # break

            # for i, val in enumerate(fire_data[0].keys()):
            #     print("fire key: ", val, " len: ", len(fire_data[0][val]))
            # # for i, val in enumerate(nonfire_data[0].keys()):
            #     print("nonfire key: ", val, " len: ", len(nonfire_data[0][val]))

            if model is None:
                X, X_second, X_fft, y_train = np.array([]), np.array([]), np.array([]), np.array([])
            count = 0
            while fires_left > 0:
                if X.size < 2: #initial build
                    print("Initial build")
                    X, X_second, X_fft, y_train, fire_data, nonfire_data = util.firstDataset(fire_data, nonfire_data)
                    print("X init: ", X.shape)
                    XA_test, XA_second, XA_fft, yA_test, nonfire_data, fire_data = util.nextDataset(fire_data, nonfire_data)
                    print("XA init: ", XA_test.shape)
                else: #add to it
                    print("Next Build")
                    X, X_second, X_fft, y_train, fire_data, nonfire_data = util.activeDataset(fire_data, nonfire_data, X, X_second, X_fft, y_train, XA_test, XA_second, XA_fft, yA_test, y_predict)
                    XA_test, XA_second, XA_fft, yA_test, nonfire_data, fire_data = util.nextDataset(fire_data, nonfire_data)

                print("Training on: ", X.shape)
                print("y: ", y_train.shape)
                fires = np.count_nonzero(y_train[:,1])
                nonfires = np.count_nonzero(y_train[:,0])
                print("FIRES: ", fires, "   NONFIRES: ", nonfires)
                mod = getModel(leave_one_out, X, X_second, X_fft, y_train, model)

                print("Testing with: ", XA_test.shape)
                if XA_test.size < 2 and test_experiments == 2:
                    X, X_second, X_fft, y_train = util.addFinalData(X, X_second, X_fft, y_train, fire_data, nonfire_data) #take out add final
                    fires = np.count_nonzero(y_train[:,1])
                    nonfires = np.count_nonzero(y_train[:,0])
                    print("breaking FIRES: ", fires, "   NONFIRES: ", nonfires)
                    break
                elif XA_test.size < 2 and test_experiments < 2:
                    test_experiments+=1
                    _, nonfire_data, _, _, _, _, _, _, _, _ = process_dat.getRangeProfileSamples(leave_one_out='networks_lab01', hybrid=True, test_experiments=test_experiments)
                    print("Returned from next data grab")
                else:
                    y_predict = mod.active_predict(XA_test, XA_second, XA_fft)

                fires_left = util.getFiresLeft(fire_data[0])
                print("FIRES LEFT: ", fires_left)
                nonfires_left = util.getFiresLeft(nonfire_data[0])
                print("NONFIRES LEFT: ", nonfires_left)

                np.save("X.npy", X)
                np.save("X_second.npy", X_second)
                np.save("X_fft.npy", X_fft)
                np.save("y_train.npy", y_train)

            # print("Saving")
            # np.save("X.npy", X)
            # np.save("X_second.npy", X_second)
            # np.save("X_fft.npy", X_fft)
            # np.save("y_train.npy", y_train)


        mod = getModel(leave_one_out, X, X_second, X_fft, y_train, model, save=True)

        y_preds = {}
        print("keys: ", X_test.keys())
        for count, key in enumerate(X_test.keys()):
            x = X_test[key]
            if x.size > 2 and key > 22:
                print("Predicting bin: ", key, " with shape: ", x.shape)
                y_p = mod.predict(x, X_test_second[key], leave_one_out_X_fft[key], second_loo_X_fft[key])
                y_preds[key] = y_p
            else:
                print("Distance ", key, " doesn't have any significant samples")
                y_preds[key] = np.array([])

        final_result, real_fire_alarms = TimeSeriesTest(y_test, y_preds, leave_one_out, ignition_sample, all_skipped_samples)

        if type(X_test) is dict:
            total_distance+=final_result.fire_distance
            # total_latency+=final_result.latency
            # total_false_alarms+=len(final_result.false_alarms)
            total_latency = 100000
            false_alarm_times = []
            for r in real_fire_alarms:
                print(r)
                if not r.fire:
                    if r.time not in false_alarm_times:
                        false_alarm_times.append(r.time)
                        # if r.time > 630: # out of the baseline
                        total_false_alarms+=1
                if r.latency < total_latency:
                    total_latency = r.latency
        else:
            full_TP+=final_result.TP
            full_TN+=final_result.TN
            full_FN+=final_result.FN
            full_FP+=final_result.FP


    if type(X_test) is dict:
        print("Average Bin:               ", total_distance/repeats)
        print("Average Distance (meters): ", (total_distance*(0.044))/repeats)
        print("Known fire bins:           ", leave_one_out_fires)
        # if (total_latency/repeats) > 5000:
        #     print('==== NO ALARM ====')
        # else:
        print("Average Latency:           ", total_latency/repeats)
        print("Average False Alarms:      ", total_false_alarms/repeats)
    else:
        print("Total TP: ", full_TP)
        print("Total TN: ", full_TN)
        print("Total FP: ", full_FP)
        print("Total FN: ", full_FN)
        print("Samples: ", X_test.shape[0])
        fires = 0
        for i in y_test:
            if i[0] < i[1]:
                fires+=1
        print("Fires: ", fires)
        print("Average TP: ", (100 * (full_TP/repeats)/fires), " %")
        print("Average TN: ", (100 * (full_TN/repeats)/(X_test.shape[0] - fires)), " %")
        print("Misclassified percentages:")
        print("Average FP: ", (100 * (full_FP/repeats)/(X_test.shape[0] - fires)), " %")
        print("Average FN: ", (100 * (full_FN/repeats)/fires), " %")
    print("Finished")


if __name__ == '__main__':

    if len(sys.argv) == 2:
        leave_one_out = sys.argv[1]
        mod = None
    elif len(sys.argv) == 3:
        leave_one_out = sys.argv[1]
        mod = sys.argv[2]
    else:
        leave_one_out = None
        mod = None

    run_experiment(leave_one_out, mod)
