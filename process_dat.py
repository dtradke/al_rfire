# RFire ACTIVE LEARNING
# 04/07/20
#

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
from os import listdir
import object_factory
import sample_factory
import util


#loads the data into pandas dataframe, deletes the last 5 lines to avoid anomalies
def loadData(fname=None, dirname=None):
    try:
        if dirname is None:
            path = '../../../home/dtradke/rfire_data/data/' + fname + '/'+ fname + '.csv'
        else:
            path = '../../../home/dtradke/rfire_data/data/' + dirname + '/' + fname + '/'+ fname + '.csv'
        # print("On local disk")
    except:
        if dirname is None:
            path = '../rfire_data/data/' + fname + '/'+ fname + '.csv'
        else:
            path = '../rfire_data/data/' + dirname + '/' + fname + '/'+ fname + '.csv'
        # print("On shared disk")
    data = pd.read_csv(path, index_col=False)
    return data.iloc[:-5]

def cutFat(df, model, test_experiments):
    beginning = True
    step = 250000 #75000 for training
    # if model is None or test_experiments > 0:
        # step = 100000
    # else:
    #     step = 250000
    loop_start = 0 #(step * test_experiments)

    # df = df.iloc[:-80]

    if df.shape[0] > (loop_start+step) - loop_start:
        df = df.iloc[loop_start:(loop_start+step)]
    # elif df.shape[0] > loop_start:
    else:
        df = df.iloc[loop_start:]
    print("DF shape: ", df.shape)
    if df.shape[0] == 0:
        return None


    if model is not None:
        if beginning:
            df = df.iloc[:250000]
        else:
            baseline = df.iloc[:600]
            df = df.iloc[start:(start+step)] #(start+step)
            df = pd.concat([baseline, df], axis=0)
        rows = len(df)
        seconds = rows//10
        minutes = seconds//60
        hours = minutes//60
        hour_remainder = minutes%60
        print("This is a total of: ", hours, " hours and ", hour_remainder, " minutes - ", df.shape)
    return df

def getDF(path, s):
    flag = False
    for idx, f in enumerate(sorted(listdir(path + s))):
        if f[0] is not '_':
            if f[0] is not '.':
                # print(f)
                returned_df = loadData(f, s)
                if flag == False:
                    flag = True
                    df = returned_df
                    continue

                df = pd.concat([df, returned_df])
    return df


def getRangeProfileSamples(leave_one_out=None, model=None, hybrid=False, test_experiments=0):
    print(">>>TEST EXPERIMENTS LOOP: ", test_experiments)
    if test_experiments > 0: #manually set leave_one_out to avoid crash
        leave_one_out = 'networks_lab01'

    try:
        scenarios = listdir('../../../home/dtradke/rfire_data/data/')
    except:
        scenarios = listdir('../../rfire_data/data/')
    objects = []
    experiments = []

    for s in scenarios:
        if model is not None and s != leave_one_out:
            # print('Testing... Skip: ', s)
            continue

        #Skipping wood fires
        split_scen = s.split('_')
        if 'fire' in split_scen:
            # if ('big' in split_scen or 'soot1' in split_scen or 'soot2' in split_scen or 'nonsoot' in split_scen or 'foam' in split_scen):
            if s != leave_one_out:
                # if ('big' not in split_scen and s != leave_one_out) or (s == 'big_soot1_fire_185cm_device_598cm' and s != leave_one_out) or (s == 'big_soot1_fire_385cm_device_598cm' and s != leave_one_out):
                # if 'tim' in split_scen or 'occlusion' in split_scen: #for sample-wise
                # if ('big' not in split_scen and s != leave_one_out): #all big fires
                if test_experiments > 0:
                    if s != 'big_soot1_fire_85cm_device_598cm': #choose random fire to hope it doesn't crash
                        print("Skipping: ", s)
                        continue
                # else:
                #     if 'big' in split_scen or "nonsoot" in split_scen:
                #         continue
                else:
                    if 'big' not in split_scen:
                        print("Skipping: ", s)
                        continue
        else:
            # if ('test' == split_scen[0] and s != leave_one_out) or ('networks' not in split_scen and s != leave_one_out):
            if test_experiments > 0:
                if 'test' != split_scen[0] and s != leave_one_out:
                    print("Skipping: ", s)
                    continue
                elif 'office' == split_scen[1] and s != leave_one_out:
                    print("Skipping: ", s)
                    continue
            else:
                if 'test' == split_scen[0] and s != leave_one_out:
                    print("Skipping: ", s)
                    continue

        if s[0] is not '_':
            if s[0] is not 'p':
                print("Loading: ", s)
                try:
                    df = loadData(s)
                except:
                    try:
                        df = getDF('../../../home/dtradke/rfire_data/data/', s)
                    except:
                        df = getDF('../rfire_data/data/', s)


                    df = df.reset_index(drop=True)

                if test_experiments > 0 and s == leave_one_out:
                    print("LOO for loop ", test_experiments, ": ", s) #added to include a short leave_one_out when training
                else:
                    df = cutFat(df, model, test_experiments)
                # print(df.shape)

                # new_objects = object_factory.makeObjects(df, s, leave_one_out)
                # objects = objects + new_objects
                # if df.shape[0] > 1000:
                if df is not None:
                    experiments.append(object_factory.makeObjects(df, s, leave_one_out))

    util.calibrateExperiments(experiments, leave_one_out) #moved below augment

    # for e in experiments:
    #     print(e)
    #     objs = e.objects
    #     for o in objs:
    #         object = objs[o]
    #         print(object)
    # exit()

    fire_data, nonfire_data, leave_one_out_X, leave_one_out_y, second_loo_X, leave_one_out_X_fft, second_loo_X_fft, all_skipped_samples = util.makeDataset(experiments, test_experiments, leave_one_out, model, hybrid)


    if leave_one_out is not None:
        for e in experiments:
            if e.name == leave_one_out:
                leave_one_out_experiment = e
                fire_start = 100000000
                for o in e.objects.keys():
                    obj = e.objects[o]
                    if obj.fire_start < fire_start and (obj.fire_start > 580):
                        fire_start = obj.fire_start
                fire_start = int(fire_start / 10)
                break
        print("Fire starting at: ", fire_start)
        return fire_data, nonfire_data, leave_one_out_X, leave_one_out_y, second_loo_X, leave_one_out_X_fft, second_loo_X_fft, leave_one_out_experiment.fire_bin_list, all_skipped_samples, fire_start
