# RFire
# 12/15/19
#
# Works with the range profile code starting with process_dat.py

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
import multiprocessing as mp
from multiprocessing import Pool
from os import listdir
import object_factory
import sample_factory
# import util


def normalizeFFT(arr):
    if arr.size < 2:
        return arr

    # reflections_idx = 18 # len 9
    reflections_idx = 10 # len 5
    # reflections_idx = 4 # len 2

    arr = arr[:,:,reflections_idx]
    to_return = []

    for a in arr:
        a = np.absolute(np.fft.fft(a))
        a = a[:128] #cut fft in half
        a = np.divide(a[1:], a[0]) #NOTE: make sure the fft is consistent... a fire might increase on the end where baseline doesnt

        maximums = np.ones(a.shape)
        maximums = np.divide(maximums, 10) # maybe change this to see how it effects predictions
        minimums = np.zeros(a.shape)

        numerator = np.subtract(a, minimums)
        denom = np.subtract(maximums, minimums)
        to_return.append(np.divide(numerator, denom))
    return np.array(to_return)

def normalize3dScenario(X, exp, globalmaxref=0, globalminref=10000000):
    if X.size < 2:
        return X

    max_pos = 255
    min_pos = 0

    space = 1 #exp.max_reflection - exp.min_reflection
    max_fluc = (exp.max_reflection - exp.min_reflection) // 2

    maxs = [exp.max_reflection, space, max_fluc, max_pos, exp.max_reflection]
    mins = [exp.min_reflection, 0, 0, min_pos, exp.min_reflection]

    maximums = []
    minimums = []
    for a in range(5):
        maximums = maximums + maxs
        minimums = minimums + mins

    maximums = np.array(maximums)
    minimums = np.array(minimums)

    for i, val in enumerate(X):

        sample_max = []
        sample_min = []
        for x, frame in enumerate(val):
            sample_max.append(maximums)
            sample_min.append(minimums)
        sample_max = np.array(sample_max)
        sample_min = np.array(sample_min)

        numerator = val.__sub__(sample_min)
        denom = sample_max.__sub__(sample_min)
        X[i] = np.divide(numerator, denom)

    return X

# Normalizes all 3d data at once
def normalizeData(X, flat, leave_one_out=False):
    global isglobalmax
    global isglobalmin

    if not leave_one_out:
        ismax = np.amax(X, axis=0)
        isglobalmax = np.amax(ismax, axis=0)
        ismin = np.amin(X, axis=0)
        isglobalmin = np.amin(ismin, axis=0)

    for i, val in enumerate(X):
        # print(val.shape)
        # print(val)
        calc_max = np.amax(val, axis=0)
        calc_min = np.amin(val, axis=0)
        # print(calc_max)
        # print(calc_min)
        max_ref = max([calc_max[0], calc_max[2], calc_max[3], calc_max[5], calc_max[6], calc_max[8], calc_max[9], calc_max[11], calc_max[12], calc_max[14]])
        min_ref = min([calc_min[0], calc_min[2], calc_min[3], calc_min[5], calc_min[6], calc_min[8], calc_min[9], calc_min[11], calc_min[12], calc_min[14]])
        # max_pos = max([calc_max[1], calc_max[4], calc_max[7], calc_max[10], calc_max[13]])
        # min_pos = min([calc_min[1], calc_min[4], calc_min[7], calc_min[10], calc_min[13]])
        max_pos = 255
        min_pos = 0

        maximums = np.array([max_ref, max_pos, max_ref, max_ref, max_pos, max_ref, max_ref, max_pos, max_ref, max_ref, max_pos, max_ref, max_ref, max_pos, max_ref]) #deleted a zero from all
        minimums = np.array([min_ref, min_pos, min_ref, min_ref, min_pos, min_ref, min_ref, min_pos, min_ref, min_ref, min_pos, min_ref, min_ref, min_pos, min_ref])
        # print(maximums)
        # print(minimums)

        sample_max = []
        sample_min = []
        for x in range(0, val.shape[0]):
            sample_max.append(maximums)
            sample_min.append(minimums)
        sample_max = np.array(sample_max)
        sample_min = np.array(sample_min)

        numerator = val.__sub__(sample_min)
        denom = sample_max.__sub__(sample_min)
        X[i] = np.divide(numerator, denom)


    # [obj.reflection[j], obj.distance, obj.normal_reflection]
    pos_inner = [calc_max[0], calc_max[1], calc_max[2], calc_max[3], calc_max[4], calc_max[5], calc_max[6], calc_max[7], calc_max[8], calc_max[9], calc_max[10], calc_max[11], calc_max[12], calc_max[13], calc_max[14]]
    pos_arr = np.array([np.array(pos_inner)])
    posmaximums = np.array([isglobalmax[0], 255, isglobalmax[0], isglobalmax[0], 255, isglobalmax[0], isglobalmax[0], 255, isglobalmax[0], isglobalmax[0], 255, isglobalmax[0], isglobalmax[0], 255, isglobalmax[0]]) #deleted a zero from all
    posminimums = np.array([isglobalmin[0], 0, isglobalmin[0], isglobalmin[0], 0, isglobalmin[0], isglobalmin[0], 0, isglobalmin[0], isglobalmin[0], 0, isglobalmin[0], isglobalmin[0], 0, isglobalmin[0]])

    posnumerator = pos_arr.__sub__(posminimums)
    posdenom = posmaximums.__sub__(posminimums)
    pos_arr = np.divide(posnumerator, posdenom)

    X[i][-1:] = pos_arr

    return X
