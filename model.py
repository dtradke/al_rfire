import tensorflow as tf
from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
from matplotlib import pyplot
from keras.models import Sequential, Model
from keras.layers import Dense, GlobalAveragePooling1D, Bidirectional, TimeDistributed, LSTM, Concatenate, Reshape
from keras.layers import Flatten, Input
from keras.layers import Dropout
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.utils import to_categorical
from keras.models import load_model
from keras.optimizers import SGD
from keras.callbacks import EarlyStopping
import time
import process_dat
import sys
import pickle





class CNNBranch(Sequential):

    def __init__(self, n_timesteps, n_features):
        super(CNNBranch, self).__init__()

        self.add(Conv1D(filters=64, kernel_size=10, activation='relu', input_shape=(n_timesteps,n_features)))
        self.add(Dropout(0.5))
        self.add(Conv1D(filters=128, kernel_size=10, activation='relu'))

        # self.add(Conv1D(filters=128, kernel_size=10, activation='relu'))

        self.add(MaxPooling1D(pool_size=2))

        self.add(Conv1D(filters=256, kernel_size=10, activation='relu')) #added
        self.add(MaxPooling1D(pool_size=2)) #added
        self.add(Dropout(0.5)) #added
        self.add(Conv1D(filters=256, kernel_size=10, activation='relu')) #added

        self.add(Flatten())
        # self.add(Dense(64, activation='relu'))
        self.add(Dense(64, activation='relu'))

        opt = SGD(lr=0.01)
        self.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

class HybridModel(Model):

    # X, X_second, X_fft, y_train ALL WRONG ORDER!!!!
    def __init__(self, trainX, trainy, X_fft, epochs=1, batch_size=32, weightsFileName=None):
        super(HybridModel, self).__init__()

        verbose, epochs, batch_size = 0, epochs, 32#500#32
        # try:
        n_timesteps, n_features, n_outputs, fft_features = trainX.shape[1], trainX.shape[2], trainy.shape[1], X_fft.shape[1]
        # except:
        #     n_timesteps, n_features, n_outputs, fft_features = 256, 25, 2, 128

        self.t_ref = Input((n_timesteps,),name='Ref_Branch')
        # self.t_fft = FFTBranch(fft_features)
        self.t_fft = Input((fft_features,),name='FFT_Branch')
        self.cnn_tm1 = CNNBranch(n_timesteps, n_features)
        self.cnn_t = CNNBranch(n_timesteps, n_features)

        lstm_input = Concatenate(name='mergedBranches', axis=1)([self.cnn_tm1.output, self.cnn_t.output])
        lstm_input = Reshape((2,64))(lstm_input)

        reflection = Dense(64, activation='relu')(self.t_ref)
        reflection = Dense(128, activation='relu')(reflection)

        fc_dense = Dense(64, activation='relu')(self.t_fft)
        # fc_dense = Dense(128, activation='relu')(fc_dense)
        # fc_dense = Dense(256, activation='relu')(fc_dense)
        fc_dense = Dense(128, activation='relu')(fc_dense)

        # fc_dense = Dense(128, activation='relu')(self.cnn_t.output)

        # lstm1 = LSTM(256, activation='relu', return_sequences=True)(lstm_input)
        # lstm2 = LSTM(256, activation='relu', return_sequences=True)(lstm1)
        lstm2 = LSTM(64, activation='relu')(lstm_input)

        ref_fft_concat = Concatenate(name='ref_fft_concat', axis=1)([reflection, fc_dense])
        ref_fft_concat = Dense(128, activation='relu')(ref_fft_concat)

        to_output = Concatenate(name='to_output', axis=1)([ref_fft_concat, lstm2])
        to_output = Dense(32, activation='relu')(to_output)
        output_dropout = Dropout(0.5)(to_output)
        out = Dense(n_outputs, activation='softmax')(output_dropout)

        super().__init__([self.t_ref, self.t_fft, self.cnn_tm1.input, self.cnn_t.input], out) #RFIRE

        # try:
        #     print(self.summary())
        # except:
        #     pass

        opt = SGD(lr=0.01)
        self.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy']) #RFIRE
        # self.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy']) #cs848

        if weightsFileName is not None:
            fname = 'AL_models/' + weightsFileName
            self.load_weights(fname)

    def fit(self, trainX, trainXsecond, trainy, X_fft, epochs, batch_size=32, verbose=0, save=False):
        val_thresh = int(trainXsecond.shape[0] * 0.9)
        # val_m1_fft = X_second_fft[val_thresh:]
        val_m1 = trainXsecond[val_thresh:]
        val_t_fft = X_fft[val_thresh:]
        val_t = trainX[val_thresh:]
        val_y = trainy[val_thresh:]
        # train_m1_fft = X_second_fft[:val_thresh]
        train_m1 = trainXsecond[:val_thresh]
        train_t_fft = X_fft[:val_thresh]
        train_t = trainX[:val_thresh]
        train_y = trainy[:val_thresh]

        reflections_idx = 10

        # train_m1 = trainXsecond
        # train_t_fft = X_fft
        # train_t = trainX
        # train_y = trainy

        train_t_ref = train_t[:,:,reflections_idx]
        val_t_ref = val_t[:,:,reflections_idx]

        tinputs = [train_t_ref, train_t_fft, train_m1, train_t]
        vinputs = [val_t_ref, val_t_fft, val_m1, val_t]
        # tinputs = [trainX_m1, trainX]
        toutputs = train_y
        voutputs = val_y

        if save:
            es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=6)
            return super().fit(tinputs, toutputs, batch_size = batch_size, epochs=300, verbose=verbose, validation_data=(vinputs, voutputs), callbacks=[es])
        return super().fit(tinputs, toutputs, batch_size = batch_size, epochs=epochs, verbose=verbose, validation_data=(vinputs, voutputs))

    def active_predict(self, testX, testXsecond, testX_fft):
        reflections_idx = 10
        testX_ref = testX[:,:,reflections_idx]
        inputs = [testX_ref, testX_fft, testXsecond, testX]
        results = super().predict(inputs)
        return results

    def predict(self, testX, testXsecond, testX_fft, testXsecond_fft):
        reflections_idx = 10
        testX_ref = testX[:,:,reflections_idx]
        second_test_ref = testXsecond[:,:,reflections_idx]

        # testX_ref = getNewInput(testX)
        # second_test_ref = getNewInput(testXsecond)

        inputs = [testX_ref, testX_fft, testXsecond, testX] #RFIRE
        # inputs = [second_test_ref, testX_ref] #CS848 project
        results = super().predict(inputs)
        return results
