# RFire
# 11/21/19
#
# Works with the range profile code starting with process_dat.py

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
from os import listdir
import math
from scipy.signal import argrelextrema


# NOTE: OG thresholds
# SIGNAL_VARIANCE_MAX_THRESHOLD = 0.25 # 0.25 for rfire_v-1.h5
# SIGNAL_VARIANCE_MIN_THRESHOLD = 0.05 #0.09
# SIGNAL_MVAR_MAX_THRESHOLD = 9 # MVAR means this is the threshold for the max_variance variable
# SIGNAL_MVAR_MIN_THRESHOLD = 0.05
#
# FFT_MAX_THRESHOLD = 0.0065
# FFT_MIN_THRESHOLD = 0.0025
# FFT_MAX_VARIANCE = 0.007
# FFT_MIN_VARIANCE = 0.0015
# FFT_SLOPE_MAX_THRESHOLD = 0
# FFT_SLOPE_MIN_THRESHOLD = -0.001
# min_slope = 0
# max_slope = -10
#
# R_SQUARED_MAX_THRESHOLD = 0.4 # MAYBE 0.25
# R_SQUARED_MIN_THRESHOLD = 0.001
# SIG_SQUARED_MAX_THRESHOLD = 1
# SIG_SQUARED_MIN_THRESHOLD = 0.001
#
# MIN_REF_THRESH = 100000
# PERIODICY_MIN_THRESH = 70
# ----------

SIGNAL_VARIANCE_MAX_THRESHOLD = 0.35 # 0.25 for rfire_v-1.h5
SIGNAL_VARIANCE_MIN_THRESHOLD = 0.01 #0.09
SIGNAL_MVAR_MAX_THRESHOLD = 10 # MVAR means this is the threshold for the max_variance variable
SIGNAL_MVAR_MIN_THRESHOLD = 0.05

FFT_MAX_THRESHOLD = 0.0085
FFT_MIN_THRESHOLD = 0.0015
FFT_MAX_VARIANCE = 0.009
FFT_MIN_VARIANCE = 0.001
FFT_SLOPE_MAX_THRESHOLD = 0
FFT_SLOPE_MIN_THRESHOLD = -0.002
min_slope = 0
max_slope = -10

R_SQUARED_MAX_THRESHOLD = 0.5 # MAYBE 0.25
R_SQUARED_MIN_THRESHOLD = 0.001
SIG_SQUARED_MAX_THRESHOLD = 1
SIG_SQUARED_MIN_THRESHOLD = 0.001

MIN_REF_THRESH = 100000
PERIODICY_MIN_THRESH = 50


def makeLine(arr, reg):
    y = []

    for a, val in enumerate(arr):
        y.append((reg[0] * a) + reg[1])
    return y


def generateSample(exp, obj, start_time, end_time, fire):
    window = []

    space = exp.max_reflection - exp.min_reflection

    time = np.linspace(0, 1, (end_time - start_time))
    pm2 = np.poly1d(np.polyfit(time, exp.objects[obj.distance - 2].reflection[start_time:end_time], 3))
    pm1 = np.poly1d(np.polyfit(time, exp.objects[obj.distance - 1].reflection[start_time:end_time], 3))
    p = np.poly1d(np.polyfit(time, obj.reflection[start_time:end_time], 3))
    pp1 = np.poly1d(np.polyfit(time, exp.objects[obj.distance + 1].reflection[start_time:end_time], 3))
    pp2 = np.poly1d(np.polyfit(time, exp.objects[obj.distance + 2].reflection[start_time:end_time], 3))

    count = 0
    for j in range(start_time, end_time):

        l_space = abs(obj.reflection[j] - obj.normal_reflection) / space
        lm1_space = abs(exp.objects[obj.distance - 1].reflection[j] - exp.objects[obj.distance - 1].normal_reflection) / space
        lm2_space = abs(exp.objects[obj.distance - 2].reflection[j] - exp.objects[obj.distance - 2].normal_reflection) / space
        lp1_space = abs(exp.objects[obj.distance + 1].reflection[j] - exp.objects[obj.distance + 1].normal_reflection) / space
        lp2_space = abs(exp.objects[obj.distance + 2].reflection[j] - exp.objects[obj.distance + 2].normal_reflection) / space

        #for extremas values: [distance_to_last_max, time_since_last_max, distance_to_last_min, time_to_last_min]
        l = [obj.reflection[j], l_space, abs(obj.reflection[j] - p(time)[count]), obj.distance, obj.normal_reflection] #add the extremas
        l_one_closer = [exp.objects[obj.distance - 1].reflection[j], lm1_space, abs(exp.objects[obj.distance - 1].reflection[j] - pm1(time)[count]), exp.objects[obj.distance - 1].distance, exp.objects[obj.distance - 1].normal_reflection]
        l_two_closer = [exp.objects[obj.distance - 2].reflection[j], lm2_space, abs(exp.objects[obj.distance - 2].reflection[j] - pm2(time)[count]), exp.objects[obj.distance - 2].distance, exp.objects[obj.distance - 2].normal_reflection]
        l_one_further = [exp.objects[obj.distance + 1].reflection[j], lp1_space, abs(exp.objects[obj.distance + 1].reflection[j] - pp1(time)[count]), exp.objects[obj.distance + 1].distance, exp.objects[obj.distance + 1].normal_reflection]
        l_two_further = [exp.objects[obj.distance + 2].reflection[j], lp2_space, abs(exp.objects[obj.distance + 2].reflection[j] - pp2(time)[count]), exp.objects[obj.distance + 2].distance, exp.objects[obj.distance + 2].normal_reflection]

        full_vector = np.array(l_two_closer + l_one_closer + l + l_one_further + l_two_further)
        window.append(full_vector)
        count+=1
    return np.array(window)


def makeZeroSample(window_size):
    arr = []
    for i in range(window_size):
        sample = []
        for j in range(15):
            sample.append(0)
        arr.append(np.array(sample))
    return np.array(arr)

def getVariance(lst, fft_bool=False):
    lst = np.array(lst)

    if fft_bool:
        degree = 1
        lst_fft = np.absolute(np.fft.fft(lst))
        fft = np.divide(lst_fft[1:], lst_fft[0])
        lst = fft[1:150] #was 5
        x_axis_fft = list(range(len(lst)))
    else:
        degree = 8

    # this gets the signal variance (percentage of signal strength instead of total number)
    # fires are around 0.10 < fire < 0.17
    time = np.linspace(0, len(lst), len(lst))
    p = np.poly1d(np.polyfit(time, lst, degree))
    mean = p(time)

    variance_total = 0
    # cur_max = 0
    for count, f in enumerate(lst):
        if fft_bool:
            variance_total+= abs(f - mean[count])
        else:
            if f == 0:
                cur = mean[count]
            else:
                cur = (abs(f - mean[count]) / f)
            variance_total+= cur
            # if cur > cur_max:
            #     cur_max = cur

    # cur_max = np.amax(np.absolute(np.gradient(lst[::2]))) / np.mean(np.absolute(np.gradient(lst[::2])))
    try:
        cur_max = max([ np.amax(np.absolute(np.diff(lst))) / np.mean(np.absolute(np.diff(lst))), np.amax(np.absolute(np.diff(lst[::2]))) / np.mean(np.absolute(np.diff(lst[::2])))])
    except:
        cur_max = 10000
    return variance_total/(lst.shape[0]), cur_max, mean


def getSlope(lst):
    lst = np.array(lst)
    lst_fft = np.absolute(np.fft.fft(lst))
    fft = np.divide(lst_fft[1:], lst_fft[0])
    fft = fft[1:150]

    time = np.linspace(0, len(fft), len(fft))
    fft_p = np.poly1d(np.polyfit(time, fft, 1))
    slope = fft_p[1]
    return slope

def getRandSigma(lst):
    lst = np.array(lst)
    lst_fft = np.absolute(np.fft.fft(lst))
    fft = np.divide(lst_fft[1:], lst_fft[0])
    fft = fft[1:150]

    time = np.linspace(0, len(fft), len(fft))
    fft_p = np.poly1d(np.polyfit(time, fft, 1))
    fft_linear_fit = fft_p(time)

    # sigma^2
    sum = 0
    for c in lst:
        if c == 0:
            sum+=((c - np.mean(lst))**2)
        else:
            sum+=(((c - np.mean(lst)) / c)**2)
    sigma_squared = sum / lst.shape[0]

    s_tot = 0
    s_res = 0
    for fft_count, f in enumerate(fft):
        s_tot += ((f - np.mean(fft))**2)
        s_res += ((f - fft_linear_fit[fft_count])**2)

    if s_tot != 0:
        div = (s_res / s_tot)
    else:
        div = 0
    R_squared = (1 - div)

    return R_squared, sigma_squared


def getPeriodicy(lst, fit):
    x = np.linspace(0, len(lst), len(lst))

    if lst[0] > fit[0]:
        prev_max = "lst"
        cur_max = "lst"
    else:
        prev_max = "fit"
        cur_max = "fit"

    crosses = 0
    for i, val in enumerate(x):
        prev_max = cur_max
        if lst[i] > fit[i]:
            cur_max = "lst"
        if fit[i] > lst[i]:
            cur_max = "fit"
        if prev_max != cur_max:
            crosses+=1

    return crosses


def getFFTAvg(lst):
    lst = np.array(lst)
    lst_fft = np.absolute(np.fft.fft(lst))
    fft = np.divide(lst_fft[1:], lst_fft[0])
    fft = fft[1:150]

    max_idx = argrelextrema(fft, np.greater)
    min_idx = argrelextrema(fft, np.less)
    maxs = fft[argrelextrema(fft, np.greater)[0]]
    mins = fft[argrelextrema(fft, np.less)[0]]

    try:
        if max_idx[0][0] < min_idx[0][0]:
            a = maxs
            b = mins
        else:
            a = mins
            b = maxs

        c = np.empty((a.size + b.size,), dtype=a.dtype)
        c[0::2] = a
        c[1::2] = b

    except:
        if max_idx[0].size == 0:
            c = mins
        elif min_idx[0].size == 0:
            c = maxs
        else:
            c = np.array([])

    total_diff = 0
    count = 0
    for key, val in enumerate(c):
        try:
            total_diff+=abs(val - c[key+1])
            count+=1
        except:
            pass

    try:
        avg = total_diff/count
    except:
        avg = 0
    return avg

def burningExperiment(exp, obj, leave_one_out_bool, hybrid):
    global min_slope
    global max_slope

    fire_samples = []
    nonfire_samples = []
    skipped_samples = []
    hybrid_fire, hybrid_nofire = [], []
    start_time = 0
    end_time = 256 #150
    step = 10
    count = 0
    while end_time < len(obj.reflection):
        sample = []

        if not leave_one_out_bool: #training
            # if its hybrid, then get the samples before
            if hybrid:
                if count > 0:
                    if end_time > obj.fire_start and start_time < (obj.fire_start + step):
                        count+=1
                        end_time+=step
                        start_time+=step
                        continue
                    else:
                        if checkThresholds(obj.reflection[start_time:end_time]):
                            if start_time < obj.fire_start: #change to this to see how the false alarms decrease
                                fire = False
                                nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False))
                                hybrid_nofire.append(generateSample(exp, obj, (start_time - step), (end_time - step), False))

                            # if hybrid, this doesnt include the "drop" from cutting the person
                            elif (start_time - (2 * step)) > obj.fire_start:
                                fire = True
                                fire_samples.append(generateSample(exp, obj, start_time, end_time, True))
                                hybrid_fire.append(generateSample(exp, obj, (start_time - step), (end_time - step), True))
            else:
                if end_time > obj.fire_start and start_time < obj.fire_start:
                    count+=1
                    end_time+=step
                    start_time+=step
                    continue
                else:
                    if start_time < obj.fire_start: #change to this to see how the false alarms decrease
                        fire = False
                        nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False))
                    else:
                        fire = True
                        fire_samples.append(generateSample(exp, obj, start_time, end_time, True))
        else:
            # if its hybrid, then get the samples before
            if hybrid:
                if count > 0:
                    if checkThresholds(obj.reflection[start_time:end_time]):
                        if start_time < obj.fire_start: #change to this to see how the false alarms decrease
                            # print("Not a fire yet")
                            fire = False
                            nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False))
                            hybrid_nofire.append(generateSample(exp, obj, (start_time - step), (end_time - step), False))
                        else:
                            fire = True
                            fire_samples.append(generateSample(exp, obj, start_time, end_time, True))
                            hybrid_fire.append(generateSample(exp, obj, (start_time - step), (end_time - step), True))
                    else:
                        skipped_samples.append(int(start_time/10))
            else:
                if start_time < obj.fire_start: #change to this to see how the false alarms decrease
                    fire = False
                    nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False))
                else:
                    fire = True
                    fire_samples.append(generateSample(exp, obj, start_time, end_time, True))


        count+=1
        end_time+=step
        start_time+=step


    return fire_samples, nonfire_samples, skipped_samples, hybrid_fire, hybrid_nofire

def nonburningExperiment(exp, obj, passed_start_time, passed_end_time, leave_one_out_bool, hybrid):
    nonfire_samples = []
    skipped_samples = []
    hybrid_nofire = []
    step = 10
    sample_size = 256

    # since the sample is being broken up for parallelizm, I need to change the start time.
    if passed_start_time == 0:
        start_time = 0
    else:
        start_time = (passed_start_time - sample_size) + step
    end_time = start_time + sample_size #150

    count = 0
    # while end_time < len(obj.reflection):
    while end_time <= passed_end_time and end_time <= obj.len_reflection:
        sample = []

        if not leave_one_out_bool: #training
            # if its hybrid, then get the samples before
            if hybrid:
                if start_time >= 10:
                    if checkThresholds(obj.reflection[start_time:end_time]):
                        nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False))
                        hybrid_nofire.append(generateSample(exp, obj, (start_time - step), (end_time - step), False))
            else:
                if average(obj.reflection[0:256]) > min(exp.top) or max(obj.reflection[start_time:end_time]) > min(exp.top):
                    nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False))
        # leave one out
        else:
            if hybrid:
                if start_time >= 10:
                    if checkThresholds(obj.reflection[start_time:end_time]):
                        nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False))
                        hybrid_nofire.append(generateSample(exp, obj, (start_time - step), (end_time - step), False))
                    else:
                        skipped_samples.append(int(start_time/10))
            else:
                if True:
                    nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False))
                else:
                    skipped_samples.append(int(start_time/10))
        end_time+=step
        start_time+=step
        count+=1

    return [], nonfire_samples, skipped_samples, [], hybrid_nofire

def checkThresholds(reflection_arr):
    variance, max_variance, signal_fit = getVariance(reflection_arr)
    fft_variance,_,_ = getVariance(reflection_arr, fft_bool=True)
    fft_slope = getSlope(reflection_arr)
    R_squared, sigma_squared = getRandSigma(reflection_arr)
    periodicy = getPeriodicy(reflection_arr, signal_fit)
    # if R_squared < R_SQUARED_MAX_THRESHOLD and R_squared > R_SQUARED_MIN_THRESHOLD and sigma_squared < SIG_SQUARED_MAX_THRESHOLD and sigma_squared > SIG_SQUARED_MIN_THRESHOLD:
    # if variance < SIGNAL_VARIANCE_MAX_THRESHOLD and variance > SIGNAL_VARIANCE_MIN_THRESHOLD and fft_slope < FFT_SLOPE_MAX_THRESHOLD and fft_variance < FFT_MAX_VARIANCE and fft_variance > FFT_MIN_VARIANCE and max(obj.reflection[start_time:end_time]) > MIN_REF_THRESH and periodicy > PERIODICY_MIN_THRESH:
    # if variance < SIGNAL_VARIANCE_MAX_THRESHOLD and variance > SIGNAL_VARIANCE_MIN_THRESHOLD and fft_slope < FFT_SLOPE_MAX_THRESHOLD and fft_variance < FFT_MAX_VARIANCE and fft_variance > FFT_MIN_VARIANCE: #rfire_v-1.h5
    if variance < SIGNAL_VARIANCE_MAX_THRESHOLD and variance > SIGNAL_VARIANCE_MIN_THRESHOLD and fft_slope < FFT_SLOPE_MAX_THRESHOLD and fft_variance < FFT_MAX_VARIANCE and fft_variance > FFT_MIN_VARIANCE and max_variance < SIGNAL_MVAR_MAX_THRESHOLD and periodicy > PERIODICY_MIN_THRESH: #new run
        return True
    return False

def average(lst):
    return sum(lst) / len(lst)

def makeSamples(experiment, object, start_time, end_time, leave_one_out_bool=False, hybrid=False):
    if experiment.fire:
        return burningExperiment(experiment, object, leave_one_out_bool, hybrid)
    else:
        return nonburningExperiment(experiment, object, start_time, end_time, leave_one_out_bool, hybrid)
